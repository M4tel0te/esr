package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.dataHolders.Deadline;
import edu.esr.backend.dataHolders.DeadlineType;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.Date;


public class DeadlineDialogController implements Controller {

    private final String title = "Přidání lhůty";
    private final static String fxmlName = "deadlineDialog.fxml";

    @FXML
    private TextField nameTextField;
    @FXML
    private Button saveButton;
    @FXML
    private DatePicker date;
    @FXML
    private ChoiceBox typeChoiceBox;
    @FXML
    private AnchorPane deadlineDialog;

    private Scene scene;

    private ESRBase base;
    private RootController root;
    private ESRApplication gui;
    /** Constructor for case of creating a new deadline **/
    public void init(ESRBase base, RootController root) {
        typeChoiceBox.getItems().addAll(new String[]{"Úřad - lhůta pro úřad", "Já - lhůta pro mne"});
        typeChoiceBox.getSelectionModel().select(1);
        saveButton.setOnAction(e -> editDeadline(null));
        deadlineDialog.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) {
                saveButton.fire();
            }
        });
        this.base = base;
        this.root = root;
        this.gui = base.getGui();
        scene = saveButton.getScene();
    }

    public static String getFxmlName() {
        return fxmlName;
    }

    @Override
    public String getTitle() {
        return title;
    }

    /** Constructor for case of editing an existing deadline **/
    public void init(ESRBase base, RootController root, Deadline dl) {
        typeChoiceBox.getItems().addAll(new String[]{"Úřad - lhůta pro úřad", "Já - lhůta pro mne"});
        if(dl.getType() == DeadlineType.OFFICE) typeChoiceBox.getSelectionModel().select(0);
        else typeChoiceBox.getSelectionModel().select(1);
        nameTextField.setText(dl.getName());
        date.setValue(dl.getDate());
        saveButton.setOnAction(e -> editDeadline(dl));
        deadlineDialog.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) {
                saveButton.fire();
            }
        });
        scene = saveButton.getScene();
        if(dl.getName().equals("Nová lhůta")) {
            scene.getWindow().setOnCloseRequest(e -> {
                base.getSelectedCase().getDeadlines().remove(dl);
            });
        }
        this.base = base;
        this.root = root;
        this.gui = base.getGui();


        ((Stage) scene.getWindow()).setTitle("Úprava lhůty - " + dl.getName());
    }

    private void editDeadline(Deadline dl) {
        var deadlineDate = date.getValue();
        var name = nameTextField.getText();
        var index = typeChoiceBox.getSelectionModel().getSelectedIndex();
        DeadlineType type;
        if(deadlineDate == null) {
            gui.errorDialog("Nebylo zvoleno žádné datum!");
            return;
        }
        if(name.length() < 3) {
            gui.errorDialog("Pojmenování musí být alespoň 3 znaky dlouhé!");
            return;
        }
        switch (index) {
            case 0:
                type = DeadlineType.OFFICE;
                break;
            case 1:
                type = DeadlineType.USER;
                break;
            default:
                gui.errorDialog("Je nutné zvolit druh lhůty!");
                return;
        }
        if(dl == null) {
            base.getSelectedCase().getDeadlines().add(
                    new Deadline(deadlineDate, name, type)
            );
        } else {
            dl.resetValues(deadlineDate, name, type);
        }

        root.loadDeadlines();
        scene.getWindow().hide();
    }

}
