package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.SettingsMaintainer;
import edu.esr.backend.dataHolders.Person;
import edu.esr.backend.dataHolders.xmlBinding.SendersWrapper;
import edu.esr.core.ESRApplication;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class SavedSendersController implements Controller {

    private final String title = "Správa uložených podatelů";
    private final static String fxmlName = "addFilesDialog.fxml";
    @FXML
    private Label loadedNumberLabel;
    @FXML
    private ChoiceBox<Person> sendersChoiceBox;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField birthDateTextField;
    @FXML
    private TextField streetAddressTextField;
    @FXML
    private TextField cityAddressTextField;
    @FXML
    private TextField dsIdTextField;
    @FXML
    private Button saveAndExitButton;
    @FXML
    private Button addNewSenderButton;
    @FXML
    private Button insertToCaseButton;
    @FXML
    private Button removeSelectedButton;


    private ESRBase base;
    private ESRApplication gui;
    private RootController root;

    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.gui = base.getGui();
        this.root = root;
        base.getSendersWrapper().ifPresent(w -> sendersChoiceBox.getItems().setAll(w.getSenders()));
        loadedNumberLabel.setText(String.valueOf(sendersChoiceBox.getItems().size()));
        if(sendersChoiceBox.getItems().size() == 0) {
            disableControls(true);
        }
        sendersChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
            @Override
            public void changed(ObservableValue<? extends Person> observableValue, Person oldPerson, Person newPerson) {
                if(oldPerson != null) {
                    savePersonValues(oldPerson);
                }
                displayPersonValues(newPerson);
            }

        });
        insertToCaseButton.setOnAction(e -> insertToCase());
        addNewSenderButton.setOnAction(e -> addNewSender());
        saveAndExitButton.setOnAction(e -> exit());
        removeSelectedButton.setOnAction(e -> removeSelected());
        sendersChoiceBox.getSelectionModel().select(0);
    }

    public static String getFxmlName() {
        return fxmlName;
    }
    private boolean savePersonValues(Person person) {
        if(inputsAreValid()) {
            person.setName(nameTextField.getText());
            person.setBirthDate(birthDateTextField.getText());
            person.setCityAddress(cityAddressTextField.getText());
            person.setStreetAddress(streetAddressTextField.getText());
            if(dsIdTextField.getText().length() == 7) {
                person.setDsID(dsIdTextField.getText());
            } else {
                person.setDsID(null);
            }
            return true;
        } else {
            return false;
        }
    }

    private void disableControls(boolean disable) {
        nameTextField.setDisable(disable);
        cityAddressTextField.setDisable(disable);
        streetAddressTextField.setDisable(disable);
        dsIdTextField.setDisable(disable);
        birthDateTextField.setDisable(disable);
        insertToCaseButton.setDisable(disable);
    }

    private void removeSelected() {
        var selectedPerson = sendersChoiceBox.getSelectionModel().getSelectedItem();
        if(selectedPerson != null) {
            if(sendersChoiceBox.getItems().size() > 1) {
                sendersChoiceBox.getItems().remove(selectedPerson);
                sendersChoiceBox.getSelectionModel().selectFirst();
                gui.informDialog("Podatel '" + selectedPerson+ "' byl úspěšně odstraněn!");
            } else {
                gui.errorDialog("Musí zůstat uložen alespoň jeden podatel!");
            }
        } else {
            gui.errorDialog("Nebyl zvolen žádný podatel k odstranění!");
        }
    }
    private void insertToCase() {
        var selectedPerson = sendersChoiceBox.getSelectionModel().getSelectedItem();
        if(selectedPerson != null) {
            savePersonValues(selectedPerson);
            base.getSelectedCase().setSender(selectedPerson);
            root.personWasInserted();
        } else {
            gui.errorDialog("Není korektně zvolen žádný podatel pro vložení!");
        }
    }
    private void exit() {
        if(inputsAreValid()) {
            savePersonValues(sendersChoiceBox.getSelectionModel().getSelectedItem());
            sendersChoiceBox.getSelectionModel().selectFirst();
            base.setSendersWrapper(new SendersWrapper(sendersChoiceBox.getItems()));
            SettingsMaintainer.saveDefaultSenders(base);
            saveAndExitButton.getScene().getWindow().hide();
        }
    }

    private boolean inputsAreValid() {
        if(nameTextField.getText().length() < 3) {
            gui.errorDialog("Jméno musí mít alespoň 3 znaky! Změna nebude uložena.");
            return false;
        }
        if(streetAddressTextField.getText().length() < 3 || cityAddressTextField.getText().length() <3) {
            gui.errorDialog("Adresa musí mít alespoň 3 znaky! Změna nebude uložena.");
            return false;
        }
        if(!birthDateTextField.getText().matches("(\\d?\\d)\\.(\\d?\\d).((\\d\\d)?\\d\\d)")) {
            gui.errorDialog("Datum narození musí být ve formátu X.X.XXXX! Změna nebude uložena.");
            return false;
        }
        var dsIdText = dsIdTextField.getText();
        if(dsIdText.length() > 0 && dsIdText.length() != 7 && !dsIdText.equals("Není načteno")) {
            gui.errorDialog("ID datové schránky musí mít přesně 7 znaků! Změna nebude uložena.");
            return false;
        }

        return true;

    }
    private void displayPersonValues(Person person) {
        nameTextField.setText(person.getName());
        birthDateTextField.setText(person.getBirthDate());
        streetAddressTextField.setText(person.getStreetAddress());
        cityAddressTextField.setText(person.getCityAddress());
        person.getDsID().ifPresentOrElse(ds -> dsIdTextField.setText(ds),
                () -> dsIdTextField.setText("Není načteno"));
    }

    private void addNewSender() {
        gui.informDialog("Vyplňte pole u 'Nový podatel' a zvolte 'Zavřít a uložit'.");
        disableControls(false);
        sendersChoiceBox.getItems().add(new Person("Nový podatel", "Ulice + ČP", "Město + PSČ","1.1.1111", null));
        sendersChoiceBox.getSelectionModel().selectLast();
    }
    @Override
    public String getTitle() {
        return title;
    }
}
