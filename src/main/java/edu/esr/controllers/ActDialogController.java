package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.ESRTools;
import edu.esr.backend.dataHolders.Act;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.File;
import java.time.LocalDate;
import java.util.Locale;

public class ActDialogController implements Controller {

    private final String title = "Přidání úkonu";
    private final static String fxmlName = "actDialog.fxml";

    private RootController root;
    private ESRBase base;
    private ESRApplication gui;

    @FXML
    private TextField actNameTextField, folderNameTextField;
    @FXML
    private DatePicker actDatePicker;
    @FXML
    private ChoiceBox actTypeChoiceBox;
    @FXML
    private CheckBox customPathCheckBox;
    @FXML
    private Button createButton;
    @FXML
    private AnchorPane dialogPane;

    public void init(ESRBase base, RootController root) {
        this.root = root;
        this.base = root.getGui().getBase();
        this.gui = root.getGui();

        actDatePicker.setValue(LocalDate.now());
        folderNameTextField.setDisable(true);
        actTypeChoiceBox.getItems().addAll(new String[]{"Podání", "Doručení", "Jiné"});
        actTypeChoiceBox.getSelectionModel().select(0);
        actNameTextField.requestFocus();
        actNameTextField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
            if(!customPathCheckBox.isSelected()) folderNameTextField.setText(transformToValidPath(newValue));
        }));
        customPathCheckBox.setOnAction(e -> folderNameTextField.setDisable(!customPathCheckBox.isSelected()));
        createButton.setOnAction(e -> createNewAct());
        dialogPane.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) createButton.fire();
        });

    }

    public void init(ESRBase base, RootController root, Act toBeEdited) {
        this.root = root;
        this.base = base;
        this.gui = base.getGui();

        actDatePicker.setValue(toBeEdited.getActDate());
        folderNameTextField.setDisable(true);
        actTypeChoiceBox.getItems().addAll(new String[]{"Podání", "Doručení", "Jiné"});
        actTypeChoiceBox.getSelectionModel().select(Act.actIndexByType(toBeEdited.getActType()));
        actNameTextField.setText(toBeEdited.getName());
        actNameTextField.requestFocus();

        customPathCheckBox.setOnAction(e -> folderNameTextField.setDisable(!customPathCheckBox.isSelected()));
        folderNameTextField.setText(toBeEdited.getActPath().toFile().getName());
        createButton.setOnAction(e -> editExistingAct(toBeEdited));
        ((Stage) createButton.getScene().getWindow()).setTitle("Úprava úkonu - " + toBeEdited.getName());
        createButton.setText("Upravit");
        dialogPane.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) createButton.fire();
        });

    }

    private void createNewAct() {
        if(inputsAreValid(true)) {
            var actFolder = new File(base.getSelectedCase().getDirectory() + "/" + folderNameTextField.getText());
            if(actFolder.mkdir()) {
                base.getSelectedCase().getActList().add(
                        new Act(actNameTextField.getText(),
                                null,
                                Act.actTypeByIndex(actTypeChoiceBox.getSelectionModel().getSelectedIndex()),
                                actFolder.toPath(),
                                actDatePicker.getValue())
                );
                root.reloadActs();
                root.getActsListView().getSelectionModel().select(root.getActsListView().getItems().size() - 1);
                createButton.getScene().getWindow().hide();

            } else gui.errorDialog("Nebylo možno vyvořit složku '" + actFolder.getPath() + "'.");
        }
    }

    private void editExistingAct(Act toBeEdited) {
        if(inputsAreValid(false)) {
            var newFolder = new File(base.getSelectedCase().getDirectory() + "/" + folderNameTextField.getText());

            if(newFolder.toPath().compareTo(toBeEdited.getActPath()) != 0) {
                var createNew = gui.yesOrNoDialog("Vytvoření nové složky pro existující úkon",
                        "Byla změněna složka pro úkon '" + toBeEdited.getName() + "' z '" + toBeEdited.getActPath().toString() + "' na '"
                + newFolder.getPath() + "'. Přejete si skutečně založit novou složku pro existující soubor? Původní složka nebude smazána, ale " +
                                "soubory ze staré složky nebudu do nové přesunuty.", "Ano", "Ne");
                if(createNew) {
                    if(newFolder.mkdir()) toBeEdited.setActPath(newFolder.toPath());
                    else gui.errorDialog("Nebylo možné vytvořit složku '" + newFolder.getPath() + "'. Úkonu bude zachována stará.");
                }
            }
            toBeEdited.setName(actNameTextField.getText());
            toBeEdited.setActDate(actDatePicker.getValue());
            toBeEdited.setActType(Act.actTypeByIndex(actTypeChoiceBox.getSelectionModel().getSelectedIndex()));

            root.reloadActs();
            root.getActsListView().getSelectionModel().select(toBeEdited);
            createButton.getScene().getWindow().hide();
        }
    }

    private boolean inputsAreValid(boolean creatingNew) {
        if(actNameTextField.getText().length() < 3) {
            gui.errorDialog("Název úkonu musí mít alespoň 3 znaky!");
            return false;
        }
        if(actDatePicker.getValue() == null) {
            gui.errorDialog("Datum musí být vyplněné!");
            return false;
        }
        if(new File(base.getSelectedCase().getDirectory() + "/" + folderNameTextField.getText()).exists() && creatingNew) {
            gui.errorDialog("Zvolená složka již existuje a využívá ji jiný úkon!");
            return false;
        }
        if(folderNameTextField.getText().isEmpty()) {
            gui.errorDialog("Název složky úkonu nemůže být prázdný!");
            return false;
        }

        if(ESRTools.containsIllegalCharacters(folderNameTextField.getText())) {
            gui.errorDialog("Název složky úkonu obsahuje nepovolené znaky!");
            return false;
        }

        return true;
    }

    private String transformToValidPath(String str) {
        return "act_" + str.replaceAll("[\\\\/:*?\"<>|]", "").
                replaceAll("\\s+", "_").
                toLowerCase(Locale.ROOT);
    }

    public static String getFxmlName() {
        return fxmlName;
    }

    @Override
    public String getTitle() {
        return title;
    }
}
