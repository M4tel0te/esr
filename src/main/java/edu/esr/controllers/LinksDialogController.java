package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.SettingsMaintainer;
import edu.esr.backend.dataHolders.Link;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinksDialogController implements Controller {

    private final String title = "Přidání/odstranění odkazů";
    private final static String fxmlName = "linksDialog.fxml";

    @FXML
    private Button addNewLinkButton, saveChangesButton, removeLinkButton;
    @FXML
    private ListView<Link> linksListView;
    @FXML
    private TextField nameTextField, urlTextField;

    private ESRBase base;
    private ESRApplication gui;
    private RootController root;

    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.gui = base.getGui();
        this.root = root;

        removeLinkButton.setOnAction(e -> removeSelected());
        addNewLinkButton.setOnAction(e -> addNewLink());
        saveChangesButton.setOnAction(e -> saveChanges());

        (saveChangesButton.getScene().getWindow()).setOnCloseRequest(e -> {
            var quit = gui.yesOrNoDialog("Zavření dialogu úpravy odkazů",
                    "Opravdu chcete okno zavřít? Jakékoliv změny nebudou uloženy.", "Ano", "Ne");
            if(!quit) e.consume();
        });

        linksListView.getItems().addAll(base.getWebLinks());
    }

    private void removeSelected() {
        var selectedLink = linksListView.getSelectionModel().getSelectedItem();
        if(selectedLink != null) {
            linksListView.getItems().remove(selectedLink);
        } else gui.informDialog("Nebyl zvolen žádný link k odstranění!");
    }

    private void addNewLink() {
        if(verifyTextFieldsData()) {
            linksListView.getItems().add(
                    new Link(nameTextField.getText(), urlTextField.getText())
            );
            nameTextField.clear();
            urlTextField.clear();
        }
    }

    private void saveChanges() {
        base.getWebLinks().clear();
        base.getWebLinks().addAll(linksListView.getItems());
        SettingsMaintainer.saveLinks(base);
        root.reloadLinks();
        saveChangesButton.getScene().getWindow().hide();
    }

    private boolean verifyTextFieldsData() {
        if(nameTextField.getText().length() < 3) {
            gui.errorDialog("Pojmenování musí mít alespoň 3 znaky!");
            return false;
        }
        var url = urlTextField.getText();
        if(url.length() < 3) {
            gui.errorDialog("Odkaz musí mít alespoň 3 znaky!");
            return false;
        }
        /** Regex source: https://www.geeksforgeeks.org/check-if-an-url-is-valid-or-not-using-regular-expression/ **/
        String regex = "((http|https)://)(www.)?"
        + "[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]"
        + "{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(url);
        if(!m.matches()) {
            gui.errorDialog("Odkaz nesplňuje formát (https://www.example.org/)!");
            return false;
        }
        return true;
    }

    public String getTitle() {
        return title;
    }

    public static String getFxmlName() {
        return fxmlName;
    }
}
