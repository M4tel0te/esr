package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.ESRTools;
import edu.esr.backend.WorkingDataSaver;
import edu.esr.backend.dataHolders.Case;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CaseDialogController implements Controller {

    private final String title = "Přidání nového případu";
    private final static String fxmlName = "caseDialog.fxml";

    @FXML
    private TextField caseNameTextField, folderNameTextField;
    @FXML
    private DatePicker creationDatePicker;
    @FXML
    private Button createButton;
    @FXML
    private CheckBox customDirectoryCheckBox;
    @FXML
    private AnchorPane pane;

    private ESRApplication gui;
    private ESRBase base;
    private RootController root;

    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.gui = base.getGui();
        this.root = root;

        creationDatePicker.setValue(LocalDate.now());
        folderNameTextField.setDisable(true);
        customDirectoryCheckBox.setOnAction(e -> folderNameTextField.setDisable(!customDirectoryCheckBox.isSelected()));

        creationDatePicker.setOnAction(e -> folderNameTextField.setText(transformToValidPath(caseNameTextField.getText())));

        caseNameTextField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
            if(!customDirectoryCheckBox.isSelected()) folderNameTextField.setText(transformToValidPath(newValue));
        }));

        createButton.setOnAction(e -> createNewCase());
        pane.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) createButton.fire();
        });

        caseNameTextField.requestFocus();
    }

    private void createNewCase() {
        if(inputsAreValid()) {
            var newCase = new Case(caseNameTextField.getText(),
                    base.getWorkingPath() + "/" + folderNameTextField.getText(),
                    creationDatePicker.getValue());
            base.getCases().add(newCase);
            WorkingDataSaver.writeNewCase(newCase, gui);
            root.reloadCases();
            createButton.getScene().getWindow().hide();
        }
    }
    private boolean inputsAreValid() {
        if(caseNameTextField.getText().length() < 2) {
            gui.errorDialog("Název případu musí mít alespoň 2 znaky.");
            return false;
        }
        if(folderNameTextField.getText().length() < 2) {
            gui.errorDialog("Název složky musí mít alespoň 2 znaky.");
            return false;
        }

        if(ESRTools.containsIllegalCharacters(folderNameTextField.getText())) {
            gui.errorDialog("Název složky obsahuje takové znaky, které nejsou povoleny.");
            return false;
        }
        if(creationDatePicker.getValue() == null) {
            gui.errorDialog("Není zvoleno žádné datum!");
            return false;
        }
        if(new File(base.getWorkingPath() + "/" + folderNameTextField.getText()).exists()) {
            gui.errorDialog("Případ s takovou složkou již existuje!");
            return false;
        }
        return true;
    }
    private String transformToValidPath(String str) {
        return creationDatePicker.getValue().format(DateTimeFormatter.ofPattern("ddMMyy")) + "_" + str.replaceAll("[\\\\/:*?\"<>|]", "").
                replaceAll("\\s+", "_").
                toLowerCase(Locale.ROOT);

    }

    @Override
    public String getTitle() {
        return title;
    }


    public static String getFxmlName() {
        return fxmlName;
    }
}
