package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class DocumentHelpController implements Controller {

    private final String title = "Nápověda pro vytváření šablon";
    private final static String fxmlName = "documentHelpDialog.fxml";

    @FXML
    private WebView contentWebView;

    public void init(ESRBase base, RootController root) {loadTemplateHelp(base);

    }

    private void loadTemplateHelp(ESRBase base) {
        try (var helpHtmlStream = DocumentHelpController.class.getClassLoader().getResourceAsStream("templateHelp.html")) {
            var htmlPath = Paths.get(System.getenv("APPDATA") + "\\ESŘ\\help.html");
            Files.copy(helpHtmlStream, htmlPath, StandardCopyOption.REPLACE_EXISTING);
            contentWebView.getEngine().load(htmlPath.toFile().toURI().toString());
            contentWebView.getScene().getWindow().setOnCloseRequest(c -> deleteHelpHtml(htmlPath, base.getGui()));
        } catch (IOException e) {
            base.getGui().exceptionDialog("Při načítání/kopírování HTML nápovědy došlo k chybě", e);
        }
    }

    private void deleteHelpHtml(Path htmlPath, ESRApplication gui) {
        try {
            Files.delete(htmlPath);
        } catch (IOException e) {
            gui.exceptionDialog("Nastala chyba při mazání dočasně uložené HTML stránky s nápovědou", e);
        }
    }

    public static String getFxmlName() {
        return fxmlName;
    }
    @Override
    public String getTitle() {
        return title;
    }
}
