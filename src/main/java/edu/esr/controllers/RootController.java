package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.backend.WorkingDataSaver;
import edu.esr.backend.WorkingDirectoryMaintainer;
import edu.esr.backend.dataHolders.*;
import edu.esr.backend.dataHolders.xmlBinding.SendersWrapper;
import edu.esr.core.ESRApplication;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Matyáš Vňuk
 */

public class RootController {
    private ESRBase base;
    private ESRApplication gui;

    @FXML
    private ListView casesList, actsFilesListView, deadlinesListView;
    @FXML
    private ListView<Act> actsListView;
    @FXML
    private MenuBar topMenu;
    @FXML
    private TextField caseSearchField, officeTextField, senderNameTextField, senderCityTextField,
            senderStreetTextField, senderDsTextField, caseFolderNameTextField, senderBirthDateTextField,
            fileNumberTextField, officeStreetTextField, officeCityTextField, officeDsTextField;
    @FXML
    private Label caseNameLabel;
    @FXML
    private ChoiceBox statusChoiceBox, actTypeChoiceBox, deadlinesChoiceBox;
    @FXML
    private Label actNameLabel;
    @FXML
    private TextArea actDescriptionTextArea;
    @FXML
    private DatePicker actDatePicker, caseCreationDatePicker;
    @FXML
    private CheckBox senderEnabledCheckBox, expiredCheckBox, senderDsCheckBox;
    @FXML
    private Button openFileButton, openFolderButton, deleteFileButton, refreshFilesButton, addFilesButton,
            newDeadlineButton, removeDeadlineButton, editDeadlineButton, newActButton, removeActButton,
            moveUpActButton, moveDownActButton, editActButton, addCaseButton, removeCaseButton;

    public RootController() {

    }

    public void init(ESRBase base, ESRApplication gui){
        this.base = base;
        this.gui = gui;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                loadCore();
                loadCases("");
                caseSearchField.requestFocus();
            }
        });
        /** Case search listener **/
        caseSearchField.textProperty().addListener((observable, oldValue, newValue) -> {
            loadCases(newValue);
        });

        /** Switching acts **/
        actsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Act>() {
            @Override
            public void changed(ObservableValue<? extends Act> observableValue, Act oldAct, Act newAct) {
                if(newAct != null) {
                    if(oldAct != null) updateAct(oldAct);

                    fillActData(newAct);
                    reloadSelectedActFilesListView();
                }
            }

        });


        /** Deadline list double-clicked **/
        deadlinesListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getClickCount() == 2) {
                    if(deadlinesListView.getSelectionModel().getSelectedItem() != null ) openDeadlineDialog(true);
                }
            }
        });

        /** Act files list double-clicked **/
        actsFilesListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getClickCount() == 2) {
                    openSelectedActFile();
                }
            }
        });
        /** Add case button clicked **/
        addCaseButton.setOnAction(e -> gui.constructDialog(CaseDialogController.getFxmlName()));
        /** Remove case button clicked **/
        removeCaseButton.setOnAction(e -> removeSelectedCase());
        /** Switching on/off sender enabled **/
        senderEnabledCheckBox.setOnAction(e -> senderReEnabled());
        /** Switching on/off DS **/
        senderDsCheckBox.setOnAction(e -> dsReEnabled());
        /** Open file button **/
        openFileButton.setOnAction(e -> openSelectedActFile());
        /** Open act folder button **/
        openFolderButton.setOnAction(e -> openActFilesFolder());
        /** Delete selected file **/
        deleteFileButton.setOnAction(e -> deleteSelectedFile());
        /** Reload act's files **/
        refreshFilesButton.setOnAction(e -> reloadSelectedActFilesListView());
        /** Add new file(s) dialog **/
        addFilesButton.setOnAction(e -> gui.constructDialog(AddFilesController.getFxmlName()));
        /** Expired deadline checkbox clicked **/
        expiredCheckBox.setOnAction(e -> loadDeadlines());
        /** Switching between deadline types via its choicebox **/
        deadlinesChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                loadDeadlines();
            }
        });
        /** Deadline remove button clicked **/
        removeDeadlineButton.setOnAction(e -> deleteSelectedDeadline());
        /** Deadline add button clicked **/
        newDeadlineButton.setOnAction(e -> openDeadlineDialog(false));
        /** Deadline edit button clicked **/
        editDeadlineButton.setOnAction(e -> openDeadlineDialog(true));
        /** Newact button clicked **/
        newActButton.setOnAction(e -> openActDialogController(false));
        /** Editact button clicked **/
        editActButton.setOnAction(e -> openActDialogController(true));
        /** Remove act button clicked **/
        removeActButton.setOnAction(e -> deleteSelectedAct());
        /** Move act up button clicked **/
        moveUpActButton.setOnAction(e -> moveSelectedAct(true));
        /** Move act down button clicked **/
        moveDownActButton.setOnAction(e -> moveSelectedAct(false));


        statusChoiceBox.getItems().addAll(Arrays.asList(new String[]{"WIN", "LOST", "OTHER", "In progress"}));
        actTypeChoiceBox.getItems().addAll(Arrays.asList(new String[]{"Podání", "Doručení", "Jiné"}));
        deadlinesChoiceBox.getItems().addAll(Arrays.asList(new String[]{"Všechny", "Úřad", "Já"}));



        topMenu.setDisable(true);

        setBasicControlsDisabled(true);


    }

    /**
     * This method is supposed to load cases and other data such as top menu bar content.
     * This method is NOT gonna be called every time when something is changed in a concrete case.
     * It might be called after link is added for instance
     */
    private void loadCore() {
        reloadLinks();
        actsFilesListView.setDisable(true);
        actsListView.setDisable(true);
    }

    public void reloadLinks() {
        topMenu.getMenus().get(2).getItems().clear();
        base.getWebLinks().forEach(lnk -> {
            MenuItem linkItem = new MenuItem(lnk.getName());
            linkItem.setOnAction(e -> gui.getHostServices().showDocument(lnk.getUrl()));
            topMenu.getMenus().get(2).getItems().add(linkItem);
        });
        var addNewLink = new MenuItem("Přidat/odebrat odkaz");
        addNewLink.setOnAction(e -> gui.constructDialog(LinksDialogController.getFxmlName()));
        topMenu.getMenus().get(2).getItems().add(addNewLink);
    }

    /**
     * Method to load cases into the left ListView bar
     */
    private void loadCases(String filter) {
        casesList.getItems().clear();
        casesList.getItems().addAll(
                base.getCases().stream()
                        .filter(s -> s.toString().toLowerCase(Locale.ROOT).contains(filter.toLowerCase(Locale.ROOT)))
                        .collect(Collectors.toList()));
    }

    public void reloadCases() {
        loadCases(caseSearchField.getText());
    }


    @FXML
    private void loadCaseButtonPressed() {
        var selectedCase = (Case) casesList.getSelectionModel().getSelectedItem();
        if(selectedCase != null) {
            saveCase();
            base.clearSelectedCase();
            base.loadCaseData(selectedCase);
            if(base.getSelectedCase().getActList() == null) return;
            clearControls();
            reloadActs();
            fillCaseData();
            clearActData();
            setDeadlinesControlsDisabled(false);
            setActButtonsDisabled(false);
            setBasicControlsDisabled(false);
            loadDeadlines();
            enableAndClearSenderData(true);
            topMenu.setDisable(false);
            base.getSelectedCase().getSender().ifPresent(s -> fillSenderData(s));
            casesList.getSelectionModel().select(selectedCase);

        } else {
            gui.errorDialog("Nebyl zvolen žádný případ k načtení!");
        }

    }

    /**
     * This method is supposed to load case data into GUI. This method will be called only once just after case is loaded.
     */
    private void fillCaseData() {
        var selectedCase = base.getSelectedCase();
        caseNameLabel.setText(selectedCase.getName());
        caseFolderNameTextField.setText(selectedCase.getDirectory().toFile().getName());
        caseCreationDatePicker.setValue(selectedCase.getCreationDate());
        statusChoiceBox.getSelectionModel().select(selectedCase.getStatusIndex());
        statusChoiceBox.setDisable(false);
        selectedCase.getFileNumber().ifPresent(number -> fileNumberTextField.setText(number));
        if(selectedCase.getOffice() != null) {
            officeTextField.setText(selectedCase.getOffice().getName());
            officeStreetTextField.setText(selectedCase.getOffice().getStreetAddress());
            officeCityTextField.setText(selectedCase.getOffice().getCityAddress());
            selectedCase.getOffice().getDsID().ifPresent(id -> officeDsTextField.setText(id));
        }
        senderEnabledCheckBox.setDisable(false);
        senderEnabledCheckBox.setSelected(false);
        fileNumberTextField.setDisable(false);
        officeTextField.setDisable(false);
        officeStreetTextField.setDisable(false);
        officeCityTextField.setDisable(false);
        officeDsTextField.setDisable(false);
    }

    private void fillActData(Act a) {
        actNameLabel.setText(a.getName());
        actDescriptionTextArea.setText(a.getDescription());
        actDescriptionTextArea.setWrapText(true);
        actDescriptionTextArea.setDisable(false);
        actTypeChoiceBox.getSelectionModel().select(a.getTypeIndex());
        actTypeChoiceBox.setDisable(false);
        actDatePicker.setValue(a.getActDate());
        actDatePicker.setDisable(false);
    }

    private void clearActData() {
        actNameLabel.setText("-");
        actDescriptionTextArea.setText(null);
        actDescriptionTextArea.setDisable(true);
        actTypeChoiceBox.getSelectionModel().select(2);
        actDatePicker.setValue(null);
        actDatePicker.setDisable(true);
        actsFilesListView.getItems().clear();
        actsFilesListView.setDisable(true);
        actTypeChoiceBox.getSelectionModel().select(-1);
        actTypeChoiceBox.setDisable(true);
        openFileButton.setDisable(true);
        openFolderButton.setDisable(true);
    }

    public void reloadActs() {
        actsListView.getItems().clear();
        actsListView.getItems().addAll(base.getSelectedCase().getActList());
        actsListView.setDisable(false);
    }
    public void personWasInserted() {
        base.getSelectedCase().getSender().ifPresent(s -> {
            senderEnabledCheckBox.setSelected(true);
            senderReEnabled();
            fillSenderData(s);
        });
    }
    /** This method will be called any time the sender checkbox is clicked **/
    private void senderReEnabled() {
        if(!senderEnabledCheckBox.isSelected()) {
            if(!(senderNameTextField.getText().isEmpty() && senderCityTextField.getText().isEmpty()
                    && senderStreetTextField.getText().isEmpty() && senderDsTextField.getText().isEmpty()
                    && senderBirthDateTextField.getText().isEmpty())) {
                var deleteSender = gui.yesOrNoDialog("Smazání informací o podateli", "Údaje o podateli budou smazány. " +
                        "Přejete si tento krok učinit?", "Ano", "Ne");
                if(deleteSender) {
                    base.getSelectedCase().setSender(null);
                    enableAndClearSenderData(true);
                } else senderEnabledCheckBox.setSelected(true);
            } else enableAndClearSenderData(true);
        } else {
            enableAndClearSenderData(false);
            senderDsTextField.setDisable(true);
        }
    }
    /** This method will be called any time the DS checkbock is clicked (re-selected) **/
    private void dsReEnabled() {
        if(!senderDsCheckBox.isSelected()) {
            if(!senderDsTextField.getText().isEmpty()) {
                var deleteDS = gui.yesOrNoDialog("Smazání informací o datové schránce podatele", "ID datové schránky bude smazáno. " +
                        "Přejete si tento krok učinit?", "Ano", "Ne");
                if(deleteDS) {
                    senderDsTextField.clear();
                    senderDsTextField.setDisable(true);
                } else senderDsCheckBox.setSelected(true);
            } else senderDsTextField.setDisable(true);

        } else {
            senderDsTextField.setDisable(false);
        }
    }

    /**
     * This method is supposed to load data about sender to GUI. It is gonna be called only if sender is present in Case.
     */
    private void fillSenderData(Person sender) {
        enableAndClearSenderData(false);
        senderEnabledCheckBox.setSelected(true);
        senderNameTextField.setText(sender.getName());
        senderBirthDateTextField.setText(sender.getBirthDate());
        senderStreetTextField.setText(sender.getStreetAddress());
        senderCityTextField.setText(sender.getCityAddress());
        var isEmpty = sender.getDsID().isEmpty();
        if(!isEmpty) {
            senderDsCheckBox.setSelected(true);
            senderDsTextField.setText(sender.getDsID().get());
            senderDsTextField.setDisable(false);
        } else senderDsTextField.setDisable(true);
    }

    private void enableAndClearSenderData(boolean disable) {
        senderNameTextField.clear();
        senderNameTextField.setDisable(disable);
        senderStreetTextField.clear();
        senderStreetTextField.setDisable(disable);
        senderCityTextField.clear();
        senderCityTextField.setDisable(disable);
        senderDsCheckBox.setDisable(disable);
        senderDsCheckBox.setSelected(false);
        senderDsTextField.clear();
        senderDsTextField.setDisable(disable);
        senderBirthDateTextField.clear();
        senderBirthDateTextField.setDisable(disable);
    }

    public void reloadSelectedActFilesListView() {
        Act selectedAct = getSelectedAct();
        actsFilesListView.getItems().clear();
        setActFilesControlsDisabled(true);
        if(selectedAct == null) return;
        selectedAct.reloadFiles();
        if(selectedAct.getFileList() != null) {
            actsFilesListView.getItems().addAll(selectedAct.getFileList());
            setActFilesControlsDisabled(false);
        }
    }

    private void setDeadlinesControlsDisabled(boolean disabled) {
        if(disabled) deadlinesChoiceBox.getSelectionModel().clearSelection();
        else deadlinesChoiceBox.getSelectionModel().select(0);
        deadlinesListView.setDisable(disabled);
        expiredCheckBox.setDisable(disabled);
        newDeadlineButton.setDisable(disabled);
        removeDeadlineButton.setDisable(disabled);
        deadlinesChoiceBox.setDisable(disabled);
        expiredCheckBox.setSelected(!disabled);
        editDeadlineButton.setDisable(disabled);
    }

    private void setBasicControlsDisabled(boolean disabled) {
        fileNumberTextField.setDisable(disabled);
        officeCityTextField.setDisable(disabled);
        officeStreetTextField.setDisable(disabled);
        officeDsTextField.setDisable(disabled);
        officeTextField.setDisable(disabled);
        statusChoiceBox.setDisable(disabled);
        if(disabled) {
            caseNameLabel.setText("-");
            actNameLabel.setText("-");
            actTypeChoiceBox.setDisable(true);
            actDatePicker.setDisable(true);
            actDescriptionTextArea.setDisable(true);
            actsListView.setDisable(true);
            setDeadlinesControlsDisabled(true);
            setActFilesControlsDisabled(true);
            setActButtonsDisabled(true);
            topMenu.setDisable(true);
            caseFolderNameTextField.setDisable(true);
            caseCreationDatePicker.setDisable(true);
            clearControls();
        }

        senderEnabledCheckBox.setDisable(disabled);
        senderNameTextField.setDisable(disabled);
        senderStreetTextField.setDisable(disabled);
        senderCityTextField.setDisable(disabled);
        senderDsCheckBox.setDisable(disabled);
        senderDsTextField.setDisable(disabled);
        senderBirthDateTextField.setDisable(disabled);

    }

    private void clearControls() {
        statusChoiceBox.getSelectionModel().select(null);
        fileNumberTextField.clear();
        officeTextField.clear();
        officeCityTextField.clear();
        officeStreetTextField.clear();
        officeDsTextField.clear();
        caseFolderNameTextField.clear();
        caseCreationDatePicker.setValue(null);
        actsListView.getItems().clear();
        actsFilesListView.getItems().clear();
        actDescriptionTextArea.clear();
        deadlinesListView.getItems().clear();
        actTypeChoiceBox.getSelectionModel().select(null);
        actDatePicker.setValue(null);
    }

    private void setActButtonsDisabled(boolean disabled) {
        newActButton.setDisable(disabled);
        removeActButton.setDisable(disabled);
        moveUpActButton.setDisable(disabled);
        moveDownActButton.setDisable(disabled);
        editActButton.setDisable(disabled);
    }

    private void openSelectedActFile() {
        try {
            if(actsFilesListView.getSelectionModel().getSelectedIndex() > -1) {
                Desktop.getDesktop().open((File) actsFilesListView.getSelectionModel().getSelectedItem());
            } else gui.informDialog("Nebyl vybrán žádný soubor!");
        } catch (IOException e) {
            gui.exceptionDialog("Nastala chyba při otevírání souboru.", e);
        }
    }

    private void openActFilesFolder() {
        try {
            Desktop.getDesktop().open(actsListView.getSelectionModel().getSelectedItem().getActPath().toFile());
        } catch (IOException e) {
            gui.exceptionDialog("Nastala chyba při otevírání složky.", e);
        }
    }

    private void deleteSelectedFile() {
        if(actsFilesListView.getSelectionModel().getSelectedIndex() > -1) {
            var file = (ActFile) actsFilesListView.getSelectionModel().getSelectedItem();
            var delete = gui.yesOrNoDialog("Smazání souboru", "Opravdu si přejete nenávratně smazat soubor '" + file.getName() + "'?", "Ano", "Ne");
            if(delete) {
                if(!file.delete()) gui.errorDialog("Soubor nebylo možno smazat!");
                Act selectedAct = getSelectedAct();
                selectedAct.getFileList().remove(file);
                reloadSelectedActFilesListView();
            }
        } else gui.informDialog("Nebyl vybrán žádný soubor k smazání!");
    }

    /**
     * This method is supposed to aggregate act's files controls and disable/enable them.
     */
    private void setActFilesControlsDisabled(boolean disable) {
        actsFilesListView.setDisable(disable);
        openFileButton.setDisable(disable);
        openFolderButton.setDisable(disable);
        deleteFileButton.setDisable(disable);
        refreshFilesButton.setDisable(disable);
        addFilesButton.setDisable(disable);
    }

    /**
     * This method is supposed to remove content of act details and disable the fields.
     * This method is supposed to be called after an act is removed.
     */
    private void flushActDetails() {
        actTypeChoiceBox.setDisable(true);
        actTypeChoiceBox.getSelectionModel().select(-1);
        actDatePicker.setDisable(true);
        actDatePicker.setValue(null);
        actDescriptionTextArea.setDisable(true);
        actDescriptionTextArea.clear();
        actNameLabel.setText("-");
    }

    private void updateAct(Act oldAct) {
        oldAct.setDescription(actDescriptionTextArea.getText());
        oldAct.setActDate(actDatePicker.getValue());
        oldAct.setActType(Act.actTypeByIndex(actTypeChoiceBox.getSelectionModel().getSelectedIndex()));
    }


    private void openActDialogController(boolean edit) {
        var selectedAct = actsListView.getSelectionModel().getSelectedItem();
        if(edit) {
            if(selectedAct == null) {
                gui.informDialog("Není zvolen úkon k úpravě!");
            } else gui.constructDialog(ActDialogController.getFxmlName(), selectedAct);
        } else gui.constructDialog(ActDialogController.getFxmlName());
    }

    private void openDeadlineDialog(boolean edit) {
        var selectedDeadline = (Deadline) deadlinesListView.getSelectionModel().getSelectedItem();
        if(edit) {
            if(selectedDeadline == null) {
                gui.informDialog("Není zvolena lhůta k úpravě!");
            } else gui.constructDialog(DeadlineDialogController.getFxmlName(), selectedDeadline);
        } else gui.constructDialog(DeadlineDialogController.getFxmlName());
    }
    @FXML
    private void openDocumentGeneratorDialog(ActionEvent actionEvent) {
        if(isAnyActSelected()) {
            if(base.anyWordFilesLoaded()) {
                if(saveDataToCase()) gui.constructDialog(DocumentGeneratorController.getFxmlName());

            } else {
                gui.errorDialog("Nebyly načteny žádné šablony ve formát MS Word (.docx, .doc)!");
            }

        }
    }
    @FXML
    public void openDeadlineCalculatorDialog(ActionEvent actionEvent) {
        gui.constructDialog(DeadlineCalculatorController.getFxmlName());
    }

    public void createNewWorkingFolder(ActionEvent actionEvent) {
        saveCase();
        base.clearSelectedCase();
        WorkingDirectoryMaintainer.createNewWorkingDirectory(gui);
        setBasicControlsDisabled(true);
        clearControls();
        enableAndClearSenderData(true);
        loadCases("");
    }

    public void loadExistingWorkingFolder(ActionEvent actionEvent) {
        saveCase();
        base.clearSelectedCase();
        WorkingDirectoryMaintainer.choseExistingWorkingDirectory(gui);
        setBasicControlsDisabled(true);
        clearControls();
        enableAndClearSenderData(true);
        loadCases("");
    }

    private void deleteSelectedAct() {
        if(getSelectedAct() == null) {
            gui.informDialog("Nebyl vybrán žádný úkon k smazání!");
            return;
        }
        var name = getSelectedAct().getName();
        if(base.getSelectedCase().deleteAct(getSelectedAct())) {
            reloadActs();
            reloadSelectedActFilesListView();
            flushActDetails();
            gui.informDialog("Úkon '" + name + "' byl úspěšně smazán!");
        } else {
            gui.errorDialog("Nebylo možno kompletně smazat zvolený úkon! Máte k tomu dostatečná oprávnění? Není nějaký ze souborů úkonu otevřený v jiném programu?");
        }
    }

    public void loadDeadlines() {
        if(base.getSelectedCase() == null) return;
        var includeExpired = expiredCheckBox.isSelected();
        var filterIndex = deadlinesChoiceBox.getSelectionModel().getSelectedIndex();

        var allDeadlines = (ArrayList<Deadline>) base.getSelectedCase().getDeadlines();
        if(allDeadlines.size() == 0) return;
        if(!includeExpired) allDeadlines = (ArrayList<Deadline>) allDeadlines.stream().filter(d -> d.getDaysRemaining() > -1 ).collect(Collectors.toList());
        if(filterIndex == 1) allDeadlines = (ArrayList<Deadline>) allDeadlines.stream().filter(d -> d.getType() == DeadlineType.OFFICE).collect(Collectors.toList());
        else if (filterIndex == 2) allDeadlines = (ArrayList<Deadline>) allDeadlines.stream().filter(d -> d.getType() == DeadlineType.USER).collect(Collectors.toList());

        allDeadlines.sort(Comparator.comparing(Deadline::getDate));
        deadlinesListView.getItems().clear();
        deadlinesListView.getItems().addAll(allDeadlines);
        deadlinesListView.setDisable(false);
    }

    private void deleteSelectedDeadline() {
        var selectedDeadline = getSelectedDeadline();
        if(selectedDeadline != null) {
            base.getSelectedCase().getDeadlines().remove(selectedDeadline);
            deadlinesListView.getItems().remove(selectedDeadline);
        } else {
            gui.informDialog("Nebyla zvolena lhůta k odstranění.");
        }
    }

    /**
     * This method will be called in both document generating cases (blank & filled)
     */
    private boolean isAnyActSelected() {
        if(getSelectedAct() == null) {
            gui.errorDialog("Nebyl zvolen žádný úkon, do kterého by bylo možno nové podání vložit!");
            return false;
        } else return true;
    }

    private void moveSelectedAct(boolean up) {
        if(getSelectedAct() != null) {
            var selectedActIndex = actsListView.getSelectionModel().getSelectedIndex();
            if(up && selectedActIndex == 0) {
                gui.informDialog("Úkon už nelze přesunout výš.");
                actsListView.requestFocus();
                return;
            }
            if(!up && selectedActIndex == (actsListView.getItems().size() - 1)) {
                gui.informDialog("Úkon už nelze přesunout níž.");
                actsListView.requestFocus();
                return;
            }
            int newIndex;
            if(up) newIndex = selectedActIndex - 1;
            else newIndex = selectedActIndex + 1;
            Collections.swap(base.getSelectedCase().getActList(), selectedActIndex, newIndex);
            reloadActs();
            actsListView.getSelectionModel().select(newIndex);
            actsListView.requestFocus();

        } else gui.informDialog("Nebyl vybrán žádný úkon!");
    }

    public ListView<Act> getActsListView() {
        return actsListView;
    }

    public Deadline getSelectedDeadline() {
        return (Deadline) deadlinesListView.getSelectionModel().getSelectedItem();
    }

    /** This method is supposed to take all the updated data from GUI and put them into case object.
     * Since things such as deadlines and acts are managed in real time this method is supposed to
     * only care about sender and office data.
     */
    private boolean saveDataToCase() {
        /** This is important to ensure that data which might have been changed and not saved
         * are actually saved. */
        if(getSelectedAct() != null) {
            var selectedAct = getSelectedAct();
            updateAct(selectedAct);
            reloadActs();
            actsListView.getSelectionModel().select(selectedAct);
        }

        var error = verifyCaseDataComponents();
        if(error == null) {
            var cs = base.getSelectedCase();
            if(!fileNumberTextField.getText().isEmpty()) {
                cs.setFileNumber(fileNumberTextField.getText());
            }

            var idDS = (String) null;
            if(!officeDsTextField.getText().isEmpty()) idDS = officeDsTextField.getText();
            cs.setOffice(new Person(officeTextField.getText(),
                    officeStreetTextField.getText(),
                    officeCityTextField.getText(),
                    null,
                    idDS));
            cs.getOffice().emptyToNull();
            cs.setStatusByIndex(statusChoiceBox.getSelectionModel().getSelectedIndex());

            if(senderEnabledCheckBox.isSelected()) {
                var senderDS = (String) null;
                if(!senderDsTextField.getText().isEmpty()) senderDS = senderDsTextField.getText();
                cs.setSender(new Person(senderNameTextField.getText(),
                        senderStreetTextField.getText(),
                        senderCityTextField.getText(),
                        senderBirthDateTextField.getText(),
                        senderDS));
            } else cs.setSender(null);

            return true;
        } else {
            gui.errorDialog("Nastala chyba při ukládání zadaých dat - " + error);
            return false;
        }
    }

    /** This method is supposed to verify that data in textfields fulfill conditions
     * before they are saved in its Case instance.**/
    private String verifyCaseDataComponents() {
        if(fileNumberTextField.getText().length() < 3 && !fileNumberTextField.getText().isEmpty()) {
            return "spisová značka musí mít alespoň 3 znaky.";
        }
        if(officeTextField.getText() != null && officeTextField.getText().length() < 3 && !officeTextField.getText().isEmpty()) {
            return "název správního orgánu musí mít alespoň 3 znaky.";
        }
        if(officeCityTextField.getText() != null && officeCityTextField.getText().length() < 3 && !officeCityTextField.getText().isEmpty()) {
            return "adresa správního orgánu (město + PSČ) musí mít alespoň 3 znaky.";
        }
        if(officeStreetTextField.getText() != null && officeStreetTextField.getText().length() < 3 && !officeStreetTextField.getText().isEmpty()) {
            return "název správního orgánu (ulice + č.p.) musí mít alespoň 3 znaky.";
        }
        if(officeDsTextField.getText() != null && officeDsTextField.getText().length() != 7 && !officeDsTextField.getText().isEmpty()) {
            return "ID datové schránky správního orgánu musí mít přesně 7 znaků.";
        }
        if(!senderEnabledCheckBox.isSelected()) {
            return null;
        }
        if(senderNameTextField.getText().length() < 3 || senderStreetTextField.getText().length() < 3 ||
        senderCityTextField.getText().length() < 3) {
            return "jméno a adresní řádky podatele musí mít alespoň 3 znaky.";
        }
        if(senderDsCheckBox.isSelected() && senderDsTextField.getText().length() != 7)
            return "ID datové schránky podatele musí mít 7 znaků!";
        if(!senderBirthDateTextField.getText().matches("(\\d?\\d)\\.(\\d?\\d).((\\d\\d)?\\d\\d)"))
            return "datum narození podatele musí odpovídat formátu DD.MM.YYYY.";
        return null;
    }

    private void removeSelectedCase() {
        var caseToBeRemoved = (Case) casesList.getSelectionModel().getSelectedItem();
        var removedIsSelected = base.getSelectedCase() == caseToBeRemoved;
        if(caseToBeRemoved == null) {
            gui.informDialog("Není zvolen žádný případ k smazání!");
            return;
        }
        if(!base.getCases().contains(caseToBeRemoved)) {
            gui.errorDialog("Případ který chcete smazat se nenechází v bázi.");
            return;
        }
        base.removeCase(caseToBeRemoved);
        reloadCases();
        /** In case the selected case is same as that one which is being deleted, it is needed to put the GUI into
         * default state (as when it is freshly oppened).
         */
        if(removedIsSelected) {
            setBasicControlsDisabled(true);
            enableAndClearSenderData(true);
        }

    }

    @FXML
    private void openGenerateBlankDocumentDialog() {
        if(isAnyActSelected()) {
            gui.constructDialog(BlankDocumentGeneratorController.getFxmlName());
        }
    }

    @FXML
    public void openSavedSendersDialog() {
        if(base.getSendersWrapper().isPresent()) {
            gui.constructDialog("savedSendersDialog.fxml");
        } else {
            var createNew = gui.yesOrNoDialog("Vytvoření nového seznamu podatelů",
                    "Nebyl nalezel validní soubor saved-senders.xml. Přejete si vytvořit nový prázdný?",
                    "Ano", "Ne");
            /* Here it actually is not needed to copy a blank XML, if user adds any content in dialog, it will be
                just marshalled
             */
            if(createNew) {
                base.setSendersWrapper(new SendersWrapper());
                gui.constructDialog("savedSendersDialog.fxml");
            }
        }
    }

    @FXML
    private void openDocumentHelpDialog() {
        gui.constructDialog(DocumentHelpController.getFxmlName());
    }

    public void saveCase() {
        if(base.getSelectedCase() != null) {
            var saved = saveDataToCase();
            if(saved) {
                new WorkingDataSaver(base.getSelectedCase(), gui);
            }
            loadCases("");
        }

    }

    public ESRApplication getGui() {
        return gui;
    }

    public Act getSelectedAct() {
        return actsListView.getSelectionModel().getSelectedItem();
    }

}
