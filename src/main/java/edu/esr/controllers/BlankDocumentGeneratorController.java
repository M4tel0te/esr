package edu.esr.controllers;

import edu.esr.backend.DocumentGenerator;
import edu.esr.backend.ESRBase;
import edu.esr.backend.ESRTools;
import edu.esr.backend.SettingsMaintainer;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.util.Pair;

import java.io.*;
import java.nio.file.Files;


public class BlankDocumentGeneratorController implements Controller {

    private final String title = "Generování blanketního podání";
    private final static String fxmlName = "blankDocumentGeneratorDialog.fxml";

    private ESRBase base;
    private RootController root;

    @FXML
    private TextField fileNameTextField;
    @FXML
    private Button generateButton;
    @FXML
    private CheckBox fillCheckBox;
    @FXML
    private AnchorPane blankDocGeneratorPane;

    @Override
    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.root = root;
        fillCheckBox.setSelected(true);
        fileNameTextField.requestFocus();
        generateButton.setOnAction(e -> generate());
        blankDocGeneratorPane.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER) {
                generateButton.fire();
            }
        });

    }

    private void generate() {
        var filename = fileNameTextField.getText();
        if(filename.length() > 0 && !ESRTools.containsIllegalCharacters(filename)) {
            DocumentGenerator.generateBlankDocument(base, root.getSelectedAct().getActPath(),
                    filename, fillCheckBox.isSelected());
        } else {
            base.getGui().errorDialog("Název souboru musí mít alespoň 1 znak a nesmí obsahovat znaky, " +
                    "které nepovoluje operační systém!");
        }

        root.reloadSelectedActFilesListView();
        generateButton.getScene().getWindow().hide();

    }

    @Override
    public String getTitle() {
        return title;
    }

    public static String getFxmlName() { return fxmlName; }
}
