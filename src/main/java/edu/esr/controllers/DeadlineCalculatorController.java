package edu.esr.controllers;

import edu.esr.backend.DeadlineCalculator;
import edu.esr.backend.ESRBase;
import edu.esr.backend.dataHolders.Deadline;
import edu.esr.backend.dataHolders.DeadlineType;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;

public class DeadlineCalculatorController implements Controller {

    private final String title = "Kalkulačka úředních lhůt";
    private final static String fxmlName = "deadlineCalculatorDialog.fxml";

    @FXML
    private DatePicker startDatePicker, resultDatePicker;
    @FXML
    private TextArea explanationTextArea;
    @FXML
    private Button calculateButton, createnewDeadlineButton;
    @FXML
    private Spinner<Integer> lengthSpinner;

    private ESRBase base;
    private ESRApplication gui;

    @Override
    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.gui = base.getGui();

        startDatePicker.setValue(LocalDate.now());
        resultDatePicker.setDisable(true);
        explanationTextArea.setWrapText(true);
        explanationTextArea.setEditable(false);
        createnewDeadlineButton.setDisable(base.getSelectedCase() == null);
        createnewDeadlineButton.setOnAction(e -> createDeadline());
        lengthSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100,8));
        calculateButton.setOnAction(e -> calculate());

    }

    @Override
    public String getTitle() {
        return title;
    }

    private void createDeadline() {
        if(resultDatePicker.getValue() != null) {
            Deadline newDl = new Deadline(resultDatePicker.getValue(), "Nová lhůta", DeadlineType.USER);
            base.getSelectedCase().getDeadlines().add(newDl);
            createnewDeadlineButton.getScene().getWindow().hide();
            gui.constructDialog(DeadlineDialogController.getFxmlName(), newDl);
        }

        else gui.errorDialog("Nebylo určeno žádné datum pro novou lhůtu!");

    }

    private void calculate() {
        if(startDatePicker.getValue() != null) {
            var pair = DeadlineCalculator.calculate(startDatePicker.getValue(), lengthSpinner.getValue());
            resultDatePicker.setValue(pair.getKey());
            explanationTextArea.setText(pair.getValue());
        } else gui.errorDialog("Není zadáno datum počátku lhůty!");
    }

    public static String getFxmlName() {
        return fxmlName;
    }
}
