package edu.esr.controllers;

import edu.esr.backend.ESRBase;

public interface Controller {

    static String getFxmlName() {
        return null;
    }

    void init(ESRBase base, RootController root);

    String getTitle();


}
