package edu.esr.controllers;

import edu.esr.backend.ESRBase;
import edu.esr.core.ESRApplication;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public class AddFilesController implements Controller {

    private final String title = "Přidání nových souborů do úkonu";
    private final static String fxmlName = "addFilesDialog.fxml";

    @FXML
    private RadioButton copyRadioButton;
    @FXML
    private RadioButton moveRadioButton;
    @FXML
    private ListView<File> droppedFilesListView;
    @FXML
    private Button removeButton;

    private RootController rootController;
    private ESRApplication gui;

    private Scene scene;

    public void init(ESRBase base, RootController root) {
        this.scene = droppedFilesListView.getScene();
        this.rootController = root;
        this.gui = root.getGui();
        final ToggleGroup radioButtonsGroup = new ToggleGroup();
        copyRadioButton.setToggleGroup(radioButtonsGroup);
        moveRadioButton.setToggleGroup(radioButtonsGroup);
        copyRadioButton.setSelected(true);

        scene.setOnDragOver(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                if (event.getGestureSource() != scene && event.getDragboard().hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                event.consume();
            }
        });

        scene.setOnDragDropped(new EventHandler<DragEvent>() {

            @Override
            public void handle(DragEvent event) {
                var db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    droppedFilesListView.getItems().addAll(db.getFiles());
                    success = true;
                }
                event.setDropCompleted(success);

                event.consume();
            }
        });
        removeButton.setOnAction(e -> removeSelectedFile());
    }

    private void removeSelectedFile() {
        if(droppedFilesListView.getSelectionModel().getSelectedIndex() > -1) {
            droppedFilesListView.getItems().remove(droppedFilesListView.getSelectionModel().getSelectedItem());
        } else {
            gui.informDialog("Nebyl zvolen žádný soubor!");
        }
    }

    @FXML
    private void execute() {
        if(droppedFilesListView.getItems().isEmpty()) {
            gui.errorDialog("Nebyly zvoleny žádné soubory k přidání.");
            scene.getWindow().hide();
            return;
        }
        var set = new HashSet<>(droppedFilesListView.getItems());
        if(droppedFilesListView.getItems().size() > set.size()) {
            gui.informDialog("Mezi soubory, které se snažíte přidat, byly nalezeny duplikáty. Každý soubor bude přidán pouze jednou.");
        }

        if(copyRadioButton.isSelected()) {
            set.forEach(f -> {
                try {
                    Files.copy(f.toPath(), Path.of(rootController.getSelectedAct().getActPath() + "\\" + f.getName()));
                } catch (IOException e) {
                    gui.exceptionDialog("Nastala chyba při kopírování souboru '" + f.getName() + "'.", e);
                }
            });
        } else {
            set.forEach(f -> {
                try {
                    Files.move(f.toPath(), Path.of(rootController.getSelectedAct().getActPath() + "\\" + f.getName()));
                } catch (IOException e) {
                    gui.exceptionDialog("Nastala chyba při přesouvání souboru '" + f.getName() +
                            "'.", e);
                }
            });
        }

        rootController.reloadSelectedActFilesListView();
        scene.getWindow().hide();

    }

    @Override
    public String getTitle() {
        return title;
    }

    public static String getFxmlName() {
        return fxmlName;
    }
}
