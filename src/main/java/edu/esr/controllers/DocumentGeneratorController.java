package edu.esr.controllers;

import edu.esr.backend.DocumentGenerator;
import edu.esr.backend.ESRBase;
import edu.esr.backend.ESRTools;
import edu.esr.backend.dataHolders.Act;
import edu.esr.backend.dataHolders.ActFile;
import edu.esr.core.ESRApplication;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DocumentGeneratorController implements Controller {

    private final String title = "Generátor podání";
    private final static String fxmlName = "documentGeneratorDialog.fxml";

    private Act selectedAct;
    private ESRBase base;
    private ESRApplication gui;
    private RootController root;

    @FXML
    private ChoiceBox<ActFile> templatesChoiceBox;
    @FXML
    private TextArea fillingTextArea;
    @FXML
    private TextField fileNameTextField;
    @FXML
    private Button openFolderButton, generateButton;


    @Override
    public void init(ESRBase base, RootController root) {
        this.base = base;
        this.gui = base.getGui();
        this.root = root;
        this.selectedAct = root.getSelectedAct();
        templatesChoiceBox.getItems().addAll(getWordTemplates());
        templatesChoiceBox.getSelectionModel().select(0);
        fillingTextArea.setWrapText(true);
        fillingTextArea.setEditable(false);
        fillingTextArea.setText(getReport());
        generateButton.setOnAction(e -> generate());
        openFolderButton.setOnAction(e -> openTemplatesFolder());
    }

    private ArrayList<ActFile> getWordTemplates() {
        var list = new ArrayList<ActFile>();
        base.getTemplates().forEach(f -> {
            if(ESRTools.isAWordFile(f.getName())) list.add((new ActFile(f)));
        });
        return list;
    }

    private void generate() {
        var outputFilename = fileNameTextField.getText();
        if(outputFilename.length() == 0) {
            gui.errorDialog("Nebyl vyplněn název výsledného souboru!");
            return;
        }
        if(ESRTools.containsIllegalCharacters(outputFilename)) {
            gui.errorDialog("Název souboru obsahuje znaky, které operační systém nedovoluje!");
            return;
        }
        var docGen = new DocumentGenerator(base, root.getSelectedAct().getActPath(), templatesChoiceBox.getSelectionModel().getSelectedItem(), outputFilename);
        docGen.generate();

        root.reloadSelectedActFilesListView();
        openFolderButton.getScene().getWindow().hide();

    }

    private void openTemplatesFolder() {
        gui.informDialog("Nově přidané šablony budou aplikací zalistovány až po restartu!");
        try {
            Desktop.getDesktop().open(new File(System.getenv("APPDATA") + "\\ESŘ\\Šablony"));
        } catch (IOException e) {
            gui.exceptionDialog("Nastala chyba při otevírání souboru.", e);
        }
    }

    private String getReport() {
        var stringBuilder = new StringBuilder();
        stringBuilder.append("Načtený případ: " + base.getSelectedCase().getName() + "\n");
        stringBuilder.append("Spisová značka: " + nullToString(base.getSelectedCase().getFileNumber().orElse(null)));
        stringBuilder.append("Podání se bude generovat do úkonu \"" + selectedAct.getName() + "\".\n\n");
        base.getSelectedCase().getSender().ifPresentOrElse(s -> {
            stringBuilder.append("Jméno podatele: " + s.getName() + "\n");
            stringBuilder.append("Datum narození: " + s.getBirthDate() + "\n");
            stringBuilder.append("Adresa: " + s.getStreetAddress() + ", " + s.getCityAddress() + "\n");
            s.getDsID().ifPresentOrElse(ds -> stringBuilder.append("Datová schránka: " + ds + "\n"),
                    () -> stringBuilder.append("ID datové schránky není dostupné.\n"));

        }, () -> stringBuilder.append("Informace o podateli nejsou dostupné.\n"));
        var office = base.getSelectedCase().getOffice();
        stringBuilder.append("\nJméno adresáta: " + nullToString(office.getName()));
        stringBuilder.append("Ulice adresáta: " + nullToString(office.getStreetAddress()));
        stringBuilder.append("Obec adresáta: " + nullToString(office.getCityAddress()));
        stringBuilder.append("Datová schránka adresáta: " + nullToString(office.getDsID().orElse(null)));
        return stringBuilder.toString();
    }

    private String nullToString(String str) {
        if(str == null) return "nebylo vyplněno.\n";
        else return str + ".\n";
    }

    @Override
    public String getTitle() {
        return title;
    }

    public static String getFxmlName() {
        return fxmlName;
    }
}
