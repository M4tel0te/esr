package edu.esr.core;

/**
 * @author Matyáš Vňuk
 */

public class Starter {

    public static void main(String[] args) {
        ESRApplication.start(args);
    }
}
