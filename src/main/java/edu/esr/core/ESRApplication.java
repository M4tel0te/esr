package edu.esr.core;

import edu.esr.backend.ESRBase;
import edu.esr.backend.dataHolders.Act;
import edu.esr.backend.dataHolders.Deadline;
import edu.esr.controllers.ActDialogController;
import edu.esr.controllers.Controller;
import edu.esr.controllers.DeadlineDialogController;
import edu.esr.controllers.RootController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.util.Optional;

/**
 * @author Matyáš Vňuk
 * GUI class for the Appliacation
 */

public class ESRApplication extends Application {
    private Stage stage;
    private ESRBase base;
    public static void start(String[] args) {
        launch(args);
    }

    private RootController rootController;

    @Override
    public void start(Stage stage) throws IOException {
        this.stage = stage;
        base = new ESRBase(this);
        var rootLoader = new FXMLLoader();
        rootLoader.setLocation(getClass().getClassLoader().getResource("fxmls/root.fxml"));

        var root = (Parent) rootLoader.load();

        rootController = rootLoader.getController();
        rootController.init(base, this);
        var scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Evidence správních řízení");
        stage.setMinHeight(800);
        stage.setMinWidth(1065);
        addControlSaveEvent(stage);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/icon.png")));
        stage.setOnCloseRequest(e -> rootController.saveCase());

        stage.show();

    }

    public void informDialog(String message) {
        var alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informace");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public void errorDialog(String message) {
        var alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Chyba!");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public void exceptionDialog(String type, Exception e) {
        var alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Výjimka");
        alert.setHeaderText(null);
        alert.setContentText(type);


        var label = new Label("Stacktrace:");

        var textArea = new TextArea(e.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public boolean yesOrNoDialog(String title, String question, String yesTest, String noText) {
        var alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(question);

        ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText(yesTest);
        ((Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL)).setText(noText);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            return true;
        } else {
            return false;
        }
    }

    private void addControlSaveEvent(Stage stage) {
        stage.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            final KeyCombination ctrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
            @Override
            public void handle(KeyEvent keyEvent) {
                if(ctrlS.match(keyEvent)) {
                    rootController.saveCase();
                    keyEvent.consume();
                }
            }
        });
    }

    public void constructDialog(String fxmlName) {
        constructDialog(fxmlName, null);
    }
    public void constructDialog(String fxmlName, Object o) {
        Parent dialog;
        try {
            var loader = new FXMLLoader(getClass().getClassLoader().getResource("fxmls/" + fxmlName));
            dialog = loader.load();
            var stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setScene(new Scene(dialog));
            var controller = (Controller) loader.getController();
            stage.setTitle(controller.getTitle());

            if(controller instanceof DeadlineDialogController && o != null) {
                ((DeadlineDialogController) controller).init(getBase(), rootController, (Deadline) o);
            } else if (controller instanceof ActDialogController && o != null) {
                ((ActDialogController) controller).init(base, rootController, (Act) o);
            } else controller.init(getBase(), rootController);

            stage.show();
        } catch (IOException e) {
            exceptionDialog("Nastala chyba při otvírání dialogu '" + fxmlName + "'.", e);
        }

    }

    public Stage getStage() {
        return stage;
    }

    public ESRBase getBase() {
        return base;
    }

    public void setBase(ESRBase base) {
        this.base = base;
    }

    public void setWindowTitle(String caseName) {
        stage.setTitle("Evidence správních řízení - " + caseName);
    }
}
