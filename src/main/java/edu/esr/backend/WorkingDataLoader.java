package edu.esr.backend;

import edu.esr.backend.dataHolders.Act;
import edu.esr.backend.dataHolders.Case;
import edu.esr.backend.dataHolders.Deadline;
import edu.esr.backend.dataHolders.Person;
import edu.esr.backend.dataHolders.xmlBinding.CasesWrapper;
import edu.esr.core.ESRApplication;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * @author Matyáš Vňuk
 * This class is supposed to receive verified working folder path (which surely exists) and verify-load the data
 * stored in folders.
 */

public class WorkingDataLoader {

    public static void loadDataFromWorkingDirectory(Path workingPath, ESRApplication gui) {
        if(verifyEsrFile(workingPath, gui)) {
            gui.getBase().setWorkingPath(workingPath);
            loadCases(gui, workingPath);
        } else {
            var selectOther = gui.yesOrNoDialog("Chyba integrity .esr souboru",
                    "Soubor project.esr neprošel ověřením integrity. Možná s ním bylo ručně manipulováno. " +
                            "To se můžete pokusit ručně opravit. Do té doby nebude možno projekt načíst. " +
                            "\n\nPřejete si zvolit či vytvořit jinou pracovní složku?", "Ano", "Ne");
            if(selectOther) {
                WorkingDirectoryMaintainer.loadWorkingDirectory(null, gui);
            } else {
                gui.informDialog("Software bude ukončen.");
                System.exit(0);
            }
        }

    }

    /**
     * Goal of this method is nothing but validating project.esr file with its xsd.
     * This provides certainty about its integrity to subsequent methods.
     */
    private static boolean verifyEsrFile(Path workingPath, ESRApplication gui) {
        var esrFile = new File(workingPath + "\\project.esr");
        if(!esrFile.exists()) {
            gui.errorDialog("Nebylo možné nalézt soubor " + esrFile.getPath() + ". Bez toho není možné projekt načíst!");
            return false;
        }

        var tmpValidatorFile = new File(workingPath + "/tmp.xsd");
        try (var esrXsd = WorkingDataLoader.class.getClassLoader().getResourceAsStream("xsd-validators/esr-validation.xsd")) {
            /** There actually might be a situation where the xsd file is already existing **/
            Files.copy(esrXsd, tmpValidatorFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

            var builder = (DocumentBuilderFactory.newInstance()).newDocumentBuilder();

            var esrDoc = builder.parse(esrFile);
            var schema = (SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI))
                    .newSchema(new File(String.valueOf(tmpValidatorFile)));


            var validator = schema.newValidator();
            validator.validate(new DOMSource(esrDoc));
        } catch (IOException e) {
            gui.exceptionDialog("Neyblo možné zkopírovat do pracovní složky validační soubor. Máte dostatečná práva k zápisu?", e);
            return false;
        } catch (ParserConfigurationException | SAXException e) {
            gui.exceptionDialog("Nastala chyba při validaci .esr souboru (" + esrFile.getPath() + "). " +
                    "Tento soubor pravděpodobné ztratil integritu, viz. detaily.", e);
            return false;
        } finally {
            try {
                Files.delete(tmpValidatorFile.toPath());
            } catch (IOException e) {
                gui.exceptionDialog("Nastala chyba při mazání dočasného xsd souboru.", e);
            }
        }
        return true;
    }

    /**
     * Goal of this method is to load data from project.esr file, verify whether the folder of case really exists and
     * then adding it to ESRBase
     */
    private static void loadCases(ESRApplication gui, Path workingPath) {
        gui.getBase().getCases().clear();
        try {
            var jaxbContext = JAXBContext.newInstance(CasesWrapper.class);
            var unmarshaller = jaxbContext.createUnmarshaller();
            var casesWrapper = (CasesWrapper) unmarshaller.unmarshal(new File(workingPath + "/project.esr"));
            gui.getBase().setCasesWrapper(casesWrapper);
            var casesDOS = casesWrapper.getCaseDTOList();
            if(casesDOS.size() == 0) {
                gui.informDialog("V project.esr nebyly nalezeny žádné případy.");
                return;
            }
            casesDOS.forEach(c -> {
                var caseFolder = new File(workingPath + "/" + c.getCaseDirectory());
                if(caseFolder.exists() && caseFolder.isDirectory()) {
                    gui.getBase().getCases().add(
                            new Case(c.getName(), caseFolder.getPath(), c.getStatus(), c.getCreationDate())
                    );
                } else {
                    gui.informDialog("Pro případ '" + c.getName() + "' nebyla nalezena patřičná složka (" + caseFolder.getPath() + "). " +
                            "Aplikace bude spuštěna, ale data tohoto případu nebudou načteny.");
                }
            });
        } catch (JAXBException e) {
            gui.exceptionDialog("Nastala chyba při parsování project.esr pomocí JAXB", e);
        }
    }

    /**
     * Goal of this method is to load exact data about case such as acts, deadlines etc.
     */
    public static void loadCaseData(Case cs, ESRApplication gui) {
        if(verifyCaseFile(cs, gui)) {
            var dbf = DocumentBuilderFactory.newInstance();
            try {
                dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                var dB = dbf.newDocumentBuilder();

                var csDoc = dB.parse(new File(cs.getDirectory() + "/case.xml"));
                csDoc.getDocumentElement().normalize();

                /** File number load **/
                var fileNumberNode = csDoc.getElementsByTagName("fileNumber").item(0);
                if(fileNumberNode != null) {
                    cs.setFileNumber(fileNumberNode.getTextContent());
                }
                /** Sender load **/
                var senderNode = csDoc.getElementsByTagName("sender").item(0);
                if(senderNode != null) {
                    var e = (Element) senderNode;
                    var idDS = (String) null;
                    if(e.getElementsByTagName("idDS").getLength() != 0) {
                        idDS = e.getElementsByTagName("idDS").item(0).getTextContent();
                    }
                    var sender = new Person(e.getElementsByTagName("name").item(0).getTextContent(),
                            e.getElementsByTagName("streetAddress").item(0).getTextContent(),
                            e.getElementsByTagName("cityAddress").item(0).getTextContent(),
                            e.getElementsByTagName("birthDate").item(0).getTextContent(),
                            idDS);
                    cs.setSender(sender);
                }

                /** Office load **/
                var officeNode = csDoc.getElementsByTagName("office").item(0);
                if(officeNode != null) {
                    var e = (Element) officeNode;
                    String idDS = null, name = null, birthDate = null, streetAddress = null, cityAddress = null;

                    if(e.getElementsByTagName("name").getLength() != 0) {
                        name = e.getElementsByTagName("name").item(0).getTextContent();
                    }
                    if(e.getElementsByTagName("streetAddress").getLength() != 0) {
                        streetAddress = e.getElementsByTagName("streetAddress").item(0).getTextContent();
                    }
                    if(e.getElementsByTagName("cityAddress").getLength() != 0) {
                        cityAddress = e.getElementsByTagName("cityAddress").item(0).getTextContent();
                    }
                    if(e.getElementsByTagName("birthDate").getLength() != 0) {
                        birthDate = e.getElementsByTagName("birthDate").item(0).getTextContent();
                    }
                    if(e.getElementsByTagName("idDS").getLength() != 0) {
                        idDS = e.getElementsByTagName("idDS").item(0).getTextContent();
                    }

                    cs.setOffice(new Person(name, streetAddress, cityAddress, birthDate, idDS));
                } else cs.setOffice(null);
                /* Acts load */
                var actNodes = csDoc.getElementsByTagName("act");
                cs.setActList(loadActs(actNodes, cs.getDirectory(), gui));
                /* Deadlines load */
                var deadlinesNode = csDoc.getElementsByTagName("deadline");
                cs.setDeadlines(loadDeadlines(deadlinesNode));

            } catch (ParserConfigurationException | SAXException | IOException e) {
                gui.exceptionDialog("Nastala chyba při načítání dat z XML souboru (" + cs.getDirectory() + "/case.xml).", e);
            }

            gui.setWindowTitle(cs.getName());
        } else {
            gui.errorDialog("Nebylo možno načíst data pro případ '" + cs.getName() + "'. Nejspíš bylo se soubory manipulováno ručně. " +
                    "Situaci se můžete pokusit napravit.");
        }
    }

    /**
     * This method is supposed to determine whether the project file exists in its supposed direction and also check it
     * against XSD.
     */
    private static boolean verifyCaseFile(Case cs, ESRApplication gui) {
        var caseFile = new File(cs.getDirectory().toString() + "/case.xml");
        if(!caseFile.exists()) {
            gui.errorDialog("Soubor '" + caseFile.getPath() + "' neexistuje. Bez něho není možné načíst data případu.");
            return false;
        }
        var tmpValidationFile = new File(caseFile.getParent() + "/tmp.xsd");
        try (var caseXsd = WorkingDataLoader.class.getClassLoader().getResourceAsStream("xsd-validators/case-validation.xsd")) {
            Files.copy(caseXsd, tmpValidationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

            var builder = (DocumentBuilderFactory.newInstance()).newDocumentBuilder();
            var esrDoc = builder.parse(caseFile);
            var schema = (SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI))
                    .newSchema(new File(String.valueOf(tmpValidationFile)));
            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(esrDoc));
        } catch (IOException | ParserConfigurationException | SAXException e) {
            gui.exceptionDialog("Nastala chyba při validaci " + caseFile.getPath() + " (viz detaily). Případ nemůže být načten.", e);
            return false;
        } finally {
            try {
                Files.delete(tmpValidationFile.toPath());
            } catch (IOException e) {
                gui.exceptionDialog("Nastala chyba při mazání dočasného xsd souboru.", e);
            }
        }
        return true;
    }

    /**
     * Goal of this method is to load acts data and return them as a list of Acts
     */
    private static ArrayList<Act> loadActs(NodeList actNodes, Path casePath, ESRApplication gui) {
        var list = new ArrayList<Act>();
        if(actNodes.getLength() != 0) {
            for(int counter = 0; counter < actNodes.getLength(); counter++) {
                var e = (Element) actNodes.item(counter);
                var actPath = Path.of(casePath + "/" + e.getElementsByTagName("folder").item(0).getTextContent());
                var actFolder = new File(String.valueOf(actPath));

                var act = new Act(e.getElementsByTagName("name").item(0).getTextContent(),
                        e.getElementsByTagName("description").item(0).getTextContent(),
                        e.getAttribute("type"),
                        actPath,
                        e.getElementsByTagName("date").item(0).getTextContent());

                if(!(actFolder.exists() && actFolder.isDirectory())) {
                    var fixMissingFolder = gui.yesOrNoDialog(act.getName() + " - nenalezena složka souborů",
                            "U úkonu '" + act.getName() + "' nebyla nalezena složka '" + act.getActPath() +
                                    "'. Přejete si složku dodatečně vytvořit? " + "Pokud nebude složka vytvořena, budou načteny " +
                                    "informace o úkonu, ale nebude možno pracovat s jeho soubory.", "Ano", "Ne");
                    if(fixMissingFolder) {
                        actFolder.mkdir();
                        gui.informDialog("Složka '" + actFolder.getPath() + "' úspěšně vytvořena.");
                        act.reloadFiles();
                    }

                } else {
                    act.reloadFiles();
                }
                list.add(act);

            }
        }
        return list;
    }

    private static ArrayList<Deadline> loadDeadlines(NodeList deadlinesNode) {
        var list = new ArrayList<Deadline>();
        if(deadlinesNode.getLength() != 0) {
            for(int counter = 0; counter < deadlinesNode.getLength(); counter++) {
                var e = (Element) deadlinesNode.item(counter);
                list.add(new Deadline(e.getElementsByTagName("date").item(0).getTextContent(),
                        e.getElementsByTagName("name").item(0).getTextContent(),
                        e.getAttribute("type")));

            }
        }


        return list;
    }
}
