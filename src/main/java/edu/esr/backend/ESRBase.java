package edu.esr.backend;

import edu.esr.backend.dataHolders.Case;
import edu.esr.backend.dataHolders.Link;
import edu.esr.backend.dataHolders.xmlBinding.CasesWrapper;
import edu.esr.backend.dataHolders.xmlBinding.SendersWrapper;
import edu.esr.core.ESRApplication;
import javafx.util.Pair;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Matyáš Vňuk
 * Core-base class to include all the information
 */

public class ESRBase {

    private ESRApplication gui;
    private List<File> templates;
    private List<Link> webLinks;
    private List<Case> cases;
    private Path workingPath;
    private Case selectedCase;
    private CasesWrapper casesWrapper;
    private Optional<SendersWrapper> sendersWrapper;

    public ESRBase(ESRApplication gui) {
        this.gui = gui;
        gui.setBase(this);
        webLinks = new ArrayList<>();
        templates = new ArrayList<>();
        cases = new ArrayList<>();
        sendersWrapper = Optional.empty();
        /** Safe method which either initializes default settings or verifies and loads saved settings in appdata **/
        SettingsMaintainer.initLocalSettings(this);

        WorkingDirectoryMaintainer.loadWorkingDirectory(workingPath, gui);

    }

    public ESRApplication getGui() {
        return gui;
    }

    public List<Link> getWebLinks() {
        return webLinks;
    }

    public void setWorkingPath(Path workingPath) {
        this.workingPath = workingPath;
    }

    public List<File> getTemplates() {
        return templates;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void loadCaseData(Case selectedCase) {
        if(cases.contains(selectedCase)) {
            WorkingDataLoader.loadCaseData(selectedCase, gui);
            this.selectedCase = selectedCase;
        } else gui.errorDialog("Případ, který se snaží načíst není uložen v backend bázi.");

    }

    public void clearSelectedCase() {
        this.selectedCase = null;
    }

    public boolean removeCase(Case csToBeRemoved) {
        var remove = gui.yesOrNoDialog("Smazání případu '" + csToBeRemoved.getName() + "'",
                "Přejete si nenávratně smazat případ případ '" + csToBeRemoved.getName() + "'?",
                "Ano", "Ne");
        if(remove) {
            gui.informDialog("Zavřete prosím všechny programy, které by mohly používat soubory " +
                    "daného případu (např. Word, PDF Reader...), jinak nebude možno smazání provést.");
        } else return false;

        if(!csToBeRemoved.wipeFiles()) {
            gui.errorDialog("Nebylo možno smazat všechny soubory.");
            return false;
        }
        cases.remove(csToBeRemoved);
        WorkingDataSaver.deleteExistingCase(csToBeRemoved, gui);

        return true;
    }

    public Path getWorkingPath() {
        return workingPath;
    }

    public Case getSelectedCase() {
        return selectedCase;
    }

    public boolean anyWordFilesLoaded() {
        if(templates.size() > 0) {
            if(templates.stream().anyMatch(
                    f -> ESRTools.isAWordFile(f.getName()))) {
                return true;
            } else return false;
        } else return false;
    }

    public CasesWrapper getCasesWrapper() {
        return casesWrapper;
    }

    public void setCasesWrapper(CasesWrapper casesWrapper) {
        this.casesWrapper = casesWrapper;
    }

    public Optional<SendersWrapper> getSendersWrapper() {
        return sendersWrapper;
    }

    public void setSendersWrapper(SendersWrapper sendersWrapper) {
        this.sendersWrapper = Optional.of(sendersWrapper);
    }
}
