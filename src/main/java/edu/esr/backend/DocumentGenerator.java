package edu.esr.backend;

import edu.esr.backend.dataHolders.Case;
import edu.esr.core.ESRApplication;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OLE2NotOfficeXmlFileException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;

import java.io.File;;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is supposed to fill data to Word templates which have been loaded into memory after starting ESR app
 */

public class DocumentGenerator {
    private Case cs;
    private ESRBase base;
    private ESRApplication gui;
    private Path actPath;
    private File template;
    private Map<String, String> marks;
    private String outputFileName;

    public DocumentGenerator(ESRBase base, Path actPath, File template, String outputFileName) {
        this.base = base;
        this.gui = base.getGui();
        this.cs = base.getSelectedCase();
        this.actPath = actPath;
        this.template = template;
        this.marks = getFilledDataPairs();
        this.outputFileName = outputFileName;
    }

    public void generate() {
        try {
            var doc = new XWPFDocument(OPCPackage.open(template.getPath()));
            for(var p : doc.getParagraphs()) {
                var runs = p.getRuns();
                if(runs != null) {
                    for(var r : runs) {
                        var text = r.getText(0);
                        if(text != null) {
                            var marksToReplace = getMarksToReplace(text);
                            if(marksToReplace.size() > 0) {
                                for(var m : marksToReplace) {
                                    text = text.replace(m, marks.get(m));
                                }
                                r.setText(text, 0);
                            }

                        }
                    }
                }
            }
            for(var tbl : doc.getTables()) {
                for(var row : tbl.getRows()) {
                    for(var cell : row.getTableCells()) {
                        for(var p : cell.getParagraphs()) {
                            for(var r : p.getRuns()) {
                                var text = r.getText(0);
                                if(text != null) {
                                    var marksToReplace = getMarksToReplace(text);
                                    if(marksToReplace.size() > 0) {
                                        for(var m : marksToReplace) {
                                            text = text.replace(m, marks.get(m));
                                        }
                                        r.setText(text, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var fileOutputStream = new FileOutputStream(actPath + "\\" + outputFileName + ".docx");
            doc.write(fileOutputStream);
            fileOutputStream.close();
            gui.informDialog("Dokument úspěšně vygenerován!");
        } catch (IOException e) {
            gui.exceptionDialog("Chyba vstupu/výstupu při generování dokumentu ze šablony", e);
        } catch (InvalidFormatException e) {
            gui.exceptionDialog("Chyba formátu při generování dokumentu ze šablony", e);
        } catch (OLE2NotOfficeXmlFileException e) {
            gui.exceptionDialog("Snažíte se upravit šablonu ve formátu Open/Libre Office. Tato aplikace umí zpracovávat pouze " +
                    "formáty MS Word.", e);
        }

    }

    private HashMap<String, String> getFilledDataPairs() {
        var pairsMap = new HashMap<String, String>();
        /** File number **/
        cs.getFileNumber().ifPresent(s -> pairsMap.put("#file_number#", s));

        var office = cs.getOffice();
        /** Office **/
        if(office.getName() != null) {
            pairsMap.put("#office_name#", office.getName());
        }
        if(office.getStreetAddress() != null) {
            pairsMap.put("#office_street_address#", office.getStreetAddress());
        }
        if(office.getCityAddress() != null) {
            pairsMap.put("#office_city_address#", office.getCityAddress());
        }
        office.getDsID().ifPresentOrElse(id ->  pairsMap.put("#office_dsID#", "ID DS: " + id),
                () -> pairsMap.put("#office_dsID#", ""));
        /** Sender **/

        cs.getSender().ifPresent(sender -> {
            pairsMap.put("#sender_name#", sender.getName());
            pairsMap.put("#sender_street_address#", sender.getStreetAddress());
            pairsMap.put("#sender_city_address#", sender.getCityAddress());
            pairsMap.put("#sender_birth_date#", sender.getBirthDate());
            sender.getDsID().ifPresentOrElse(s -> pairsMap.put("#sender_dsID#", "ID DS: " + s),
                    () -> pairsMap.put("#sender_dsID#", ""));
        });
        /** Current date **/
        pairsMap.put("#today_date#", LocalDate.now().format(DateTimeFormatter.ofPattern("dd. MM. yyyy")));

        return pairsMap;
    }

    private List<String> getMarksToReplace(String str) {
        var m = new ArrayList<String>();
        marks.keySet().forEach(s -> {
            if(str.contains(s)) m.add(s);
        });
        return m;
    }

    public static void generateBlankDocument(ESRBase base, Path actPath, String outputFileName, boolean fill) {
        try (var inputStream = DocumentGenerator.class.getClassLoader().getResourceAsStream("Templates/Blanketní podání.docx")) {
            var templatePath = Paths.get(System.getenv("APPDATA") + "\\ESŘ\\temp_template.docx");
            Files.copy(inputStream, templatePath);
            if(fill) {
                var docGen = new DocumentGenerator(base, actPath, templatePath.toFile(), outputFileName);
                docGen.generate();
            } else {
                Files.copy(templatePath, Path.of(actPath + "\\" + outputFileName + ".docx"));
            }
            Files.delete(templatePath);
        } catch (IOException e) {
            base.getGui().exceptionDialog("Nastala chyba při generování blanketního podání", e);
        }
    }
}
