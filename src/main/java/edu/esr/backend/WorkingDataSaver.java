package edu.esr.backend;

import edu.esr.backend.dataHolders.Case;
import edu.esr.backend.dataHolders.xmlBinding.CasesWrapper;
import edu.esr.core.ESRApplication;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;


public class WorkingDataSaver {

    private Case cs;
    private ESRApplication gui;
    /** Doc is inited as a class variable because we don't want to link it to attachAsNodeWithContent() method
     * every single time.
     */
    private Document doc;


    public WorkingDataSaver(Case cs, ESRApplication gui) {
        this.cs = cs;
        this.gui = gui;
        saveCaseToFiles();
    }

    private void saveCaseToFiles() {
        saveToProjectFile();
        saveToCaseFile();
    }

    /**
     * This method is supposed to save case's core data into case.xml file
     */
    private void saveToCaseFile() {
        var docFactory = DocumentBuilderFactory.newInstance();

        try {
            var docBuilder = docFactory.newDocumentBuilder();
            this.doc = docBuilder.newDocument();

            var root = doc.createElement("case");
            doc.appendChild(root);

            /** If filenumber is present, then add it: **/
            cs.getFileNumber().ifPresent(fileNumber -> {
                attachAsNodeWithContent(root, "fileNumber", fileNumber);
            });

            /** If sender is present, then add it: **/
            cs.getSender().ifPresent(s -> {
                var senderNode = doc.createElement("sender");
                root.appendChild(senderNode);
                /* Sender name */
                attachAsNodeWithContent(senderNode, "name", s.getName());
                /* Sender streetAddress */
                attachAsNodeWithContent(senderNode, "streetAddress", s.getStreetAddress());
                /* Sender cityAddress */
                attachAsNodeWithContent(senderNode, "cityAddress", s.getCityAddress());
                /* Sender birthDate */
                attachAsNodeWithContent(senderNode, "birthDate", s.getBirthDate());
                /* Sender DS if available */
                s.getDsID().ifPresent(ds -> {
                    attachAsNodeWithContent(senderNode, "idDS", ds);
                });

            });
            /** Office **/
            var officeNode = doc.createElement("office");
            root.appendChild(officeNode);
            var office = cs.getOffice();
            if(office.getName() != null) attachAsNodeWithContent(officeNode, "name", office.getName());
            if(office.getStreetAddress() != null) attachAsNodeWithContent(officeNode, "streetAddress", office.getStreetAddress());
            if(office.getCityAddress() != null) attachAsNodeWithContent(officeNode, "cityAddress", office.getCityAddress());
            office.getDsID().ifPresent(id -> attachAsNodeWithContent(officeNode, "idDS", id));

            var actsNode = doc.createElement("acts");
            root.appendChild(actsNode);
            /* Acts: */
            cs.getActList().forEach(act -> {
                var actNode = doc.createElement("act");
                var type = doc.createAttribute("type");
                type.setValue(act.getTypesFirstLetter());
                actNode.setAttributeNode(type);
                actsNode.appendChild(actNode);

                attachAsNodeWithContent(actNode, "name", act.getName());
                String description = act.getDescription();
                attachAsNodeWithContent(actNode, "description", description);
                attachAsNodeWithContent(actNode, "date", act.getActDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                attachAsNodeWithContent(actNode, "folder", act.getActPath().toFile().getName());
            });

            var deadlinesNode = doc.createElement("deadlines");
            root.appendChild(deadlinesNode);
            /** Deadlines **/
            cs.getDeadlines().forEach(deadline -> {
                var dlNode = doc.createElement("deadline");
                var type = doc.createAttribute("type");
                type.setValue(deadline.getTypesFirstLetter());
                dlNode.setAttributeNode(type);
                deadlinesNode.appendChild(dlNode);

                attachAsNodeWithContent(dlNode, "date", deadline.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                attachAsNodeWithContent(dlNode, "name", deadline.getName());


            });

            /** Final transformation into XML **/
            var transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource src = new DOMSource(doc);
            var strResult = new StreamResult(new File(cs.getDirectory() + "/case.xml"));

            transformer.transform(src, strResult);
        } catch (ParserConfigurationException | TransformerException e) {
            gui.exceptionDialog("Nastala chyba při ukládání dat do souboru (case.xml)!", e);
        }
    }

    /**
     * This method is supposed to save basic data into so called project.esr file
     */
    private void saveToProjectFile() {
        try {
            var docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            var esrPath = gui.getBase().getWorkingPath() + "/project.esr";
            var esrDocument = docBuilder.parse(new File(esrPath));
            esrDocument.normalize();

            NodeList caseNodes = esrDocument.getElementsByTagName("case");
            for(int i = 0; i < caseNodes.getLength(); i++) {
                var caseElement = (Element) caseNodes.item(i);

                if(caseElement.getElementsByTagName("caseDirectory").item(0).getTextContent()
                        .equals(cs.getDirectory().getFileName().toString())) {
                    var nameElement = (Element) caseElement.getElementsByTagName("name").item(0);
                    nameElement.setTextContent(cs.getName());
                    var date = (Element) caseElement.getElementsByTagName("creationDate").item(0);
                    date.setTextContent(cs.getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                    caseNodes.item(i).getAttributes().getNamedItem("status").setTextContent(cs.getStatusFirstLetter());
                }
            }

            var transformer = TransformerFactory.newInstance().newTransformer();
            var src = new DOMSource(esrDocument);
            transformer.transform(src, new StreamResult(new File(esrPath)));

        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            gui.exceptionDialog("Nastala chyba při ukládání dat do souboru (project.esr)!", e);
        }

    }

    private void attachAsNodeWithContent(Element nodeToAttachTo, String nodeName, String content) {
        var toBeAdded = doc.createElement(nodeName);
        if(content != null) {
            toBeAdded.appendChild(doc.createTextNode(content));
        }
        nodeToAttachTo.appendChild(toBeAdded);
    }


    public static void writeNewCase(Case cs, ESRApplication gui) {
        var base = gui.getBase();
        if(!new File(String.valueOf(cs.getDirectory())).mkdir()) {
            gui.errorDialog("Nebylo možné vytvořit složky v - " + cs.getDirectory());
            return;
        }
        var casesWrapper = base.getCasesWrapper();
        casesWrapper.addNewCase(cs);

        try (var inputStream = WorkingDataSaver.class.getClassLoader().getResourceAsStream("default-case.xml")) {
            var jaxbContext = JAXBContext.newInstance(CasesWrapper.class);
            var marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            var esrFile = new File(base.getWorkingPath() + "\\project.esr");

            Files.copy(inputStream, Path.of(cs.getDirectory() + "\\case.xml"));
            marshaller.marshal(casesWrapper, esrFile);

            gui.informDialog("Případ '" + cs.getName() + "' byl úspěšně vytvořen!");
        } catch (JAXBException | IOException e) {
            gui.exceptionDialog("Nastala chyba při zapisování nového případu do XML pomocí JAXB", e);
        }

    }

    public static void deleteExistingCase(Case cs, ESRApplication gui) {
        var base = gui.getBase();
        var casesWrapper = base.getCasesWrapper();
        casesWrapper.removeCase(cs);

        try {
            var jaxbContext = JAXBContext.newInstance(CasesWrapper.class);
            var marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            var esrFile = new File(base.getWorkingPath() + "\\project.esr");
            marshaller.marshal(casesWrapper, esrFile);

            gui.informDialog("Případ '" + cs.getName() + "' byl úspěšně smazán!");
        } catch (JAXBException e) {
            gui.exceptionDialog("Nastala chyba při zapisování nového případu do XML pomocí JAXB", e);
        }
    }


}
