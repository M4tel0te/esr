package edu.esr.backend;

import org.w3c.dom.Node;

import java.io.File;
import java.util.regex.Pattern;

/**
 * This class was created to avoid having literally the same code on multiple places.
 */
public class ESRTools {
    /**
     * Goal of this method is to delete entire config folder including all the subfolders and subfiles
     * It is already verified that the config folder exists, therefore we dont need to check it again
     */
    public static boolean deleteDirectory(File directoryToBeDeleted) {
        var allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (var file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    /**
     * Goal of this method is to remove all the children on a node
     */
    public static void removeChildren(Node node) {
        while (node.hasChildNodes()){
            node.removeChild(node.getFirstChild());
        }
    }

    /**
     * Source https://www.baeldung.com/java-file-extension
     */

    public static boolean isAWordFile(String filename) {
        if(filename.contains(".")) {
            return filename.substring(filename.lastIndexOf(".") + 1).contains("doc");
        } else return false;
    }

    /**
     *
     * This method is supposed to check whether there are any chars which Windows doesn't allow in
     * file/folder names
     */
    public static boolean containsIllegalCharacters(String filename) {
        var pattern = Pattern.compile("[\\\\/:*?\"<>|]");
        var matcher = pattern.matcher(filename);
        return matcher.find();
    }
}
