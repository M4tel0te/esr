package edu.esr.backend;

import javafx.util.Pair;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DeadlineCalculator {

    private static List<String> holidays = new ArrayList<>();

    public static Pair<LocalDate, String> calculate(LocalDate begin, int len) {
        var messageBuilder = new StringBuilder("Skutečnost určující počátek lhůty nastala " + format(begin) + ".\n");
        messageBuilder.append("První den lhůty je " + format(begin.plusDays(1)) + ".\n");

        var finalDate = checkIfWeekend(begin.plusDays(len), messageBuilder);
        finalDate = checkIfEaster(finalDate, messageBuilder);
        finalDate = checkIfHoliday(finalDate, messageBuilder);

        messageBuilder.append("\nPoslední den ještě ve lhůtě je " + format(finalDate) + ".");
        return new Pair<>(finalDate, messageBuilder.toString());
    }

    /**
     * This method is supposed to check if the day is a weekend and transfer it in case it is a weekend
     */

    private static LocalDate checkIfWeekend(LocalDate date, StringBuilder builder) {
        if(date.getDayOfWeek() == DayOfWeek.SATURDAY) {
            var newDate = date.plusDays(2);
            builder.append("Protože je " + format(date) + " sobota, poslední den lhůty se posouvá na " + format(newDate) + ".\n");
            return newDate;
        } else if(date.getDayOfWeek() == DayOfWeek.SUNDAY) {
            var newDate = date.plusDays(1);
            builder.append("Protože je " + format(date) + " neděle, poslední den lhůty se posouvá na " + format(newDate) + ".\n");
            return newDate;
        } else return date;
    }

    private static LocalDate checkIfHoliday(LocalDate date, StringBuilder builder) {
        var newDate = date;
        while(holidays.contains(newDate.format(DateTimeFormatter.ofPattern("dd.MM")) + ".")) {
            builder.append("Protože je " + format(newDate) + " státní svátek, poslední možný den se posouvá na " + format(newDate.plusDays(1)) + "\n");
            newDate = newDate.plusDays(1);
        }
        return newDate;
    }

    private static LocalDate checkIfEaster(LocalDate date, StringBuilder builder) {
        var newDate = date;
        if(getEasterHolidays(newDate.getYear())[0].equals(newDate)) {
            newDate = newDate.plusDays(1);
            builder.append("Protože je " + format(date) + " státní svátek (Velikonoční pondělí), poslední možný den se posouvá na " + format(newDate) + ".\n");
        } else if (getEasterHolidays(newDate.getYear())[1].equals(newDate)) {
            newDate = newDate.plusDays(4);
            builder.append("Protože je " + format(date) + " státní svátek (Velký pátek), poslední možný den se posouvá na " + format(newDate) + ".\n");
        }
        return newDate;
    }

    /**
     * Carl Friedrich Gauss algo from 1800 to get easter holidays
     */
    private static LocalDate[] getEasterHolidays(int year) {
        int y = year;
        int a = y % 19;
        int b = y / 100;
        int c = y % 100;
        int d = b / 4;
        int e = b % 4;
        int g = (8 * b + 13) / 25;
        int h = (19 * a + b - d - g + 15) % 30;
        int j = c / 4;
        int k = c % 4;
        int m = (a + 11 * h) / 319;
        int r = (2 * e + 2 * j - k - h + m + 32) % 7;
        int n = (h - m + r + 90) / 25;
        int p = (h - m + r + n + 19) % 32;

        var easterSunday = LocalDate.of(year, n, p);

        return new LocalDate[]{easterSunday.plusDays(1), easterSunday.minusDays(2)};
    }

    private static String format(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public static void addHolidayDate(String date) {
        holidays.add(date);
    }

}
