package edu.esr.backend;

import edu.esr.backend.dataHolders.xmlBinding.Configuration;
import edu.esr.backend.dataHolders.xmlBinding.SendersWrapper;
import edu.esr.core.ESRApplication;
import javafx.util.Pair;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;


/**
 * @author Matyáš Vňuk
 * Class which is supposed to contain all the methods related to loading, creating and re-writing apps
 * settings saved in %appdata% folder.
 */

public class SettingsMaintainer {
    private static boolean templatesFound;
    private final static Path localSettingsPath = Paths.get(System.getenv("APPDATA") + "\\ESŘ");
    private final static Path configFilePath = Path.of(localSettingsPath + "\\config.xml");
    /**
     * Method copies default settings in case of missing it.
     * @param gui
     */
    private static void setDefaultSettings(ESRApplication gui) {
        var templatesFolder = new File(localSettingsPath+ "\\Šablony\\");
        gui.informDialog("Protože nebyla nalezena potřebná konfigurace, Budou nakopírovány základní soubory do " +
                localSettingsPath.toAbsolutePath());
        if(new File(String.valueOf(localSettingsPath)).mkdir()) {
            if(templatesFolder.mkdir()) {
                /** Templates copying **/
                getDefaultTemplatesAsStreams(gui).forEach(template -> {
                    try {
                        Files.copy((InputStream) template.getValue(),
                                Paths.get(templatesFolder.getPath() + "\\" +
                                        template.getKey().toString().substring(template.getKey().toString().lastIndexOf("/") + 1)));
                        ((InputStream) template.getValue()).close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                try {
                    var resourceStream = SettingsMaintainer.class.getClassLoader().getResourceAsStream("default-config.xml");
                    Files.copy(resourceStream, configFilePath);
                    resourceStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                gui.informDialog("Kopírování defaultní konfigurace proběhlo úspěšně!");

            }
        } else {
            gui.errorDialog("Nebylo možno vytvořit složku s defaultním nastavení v " + localSettingsPath.toAbsolutePath() + "\nProgram nelze spustit.");
            System.exit(1);
        }
    }

    /**
     * Method loads all the content included in TemplatesTable.txt and returns the file in a form of InputStream
     * @return
     */
    private static List<Pair> getDefaultTemplatesAsStreams(ESRApplication gui)  {
        var templatesList = new LinkedList<Pair>();

        try (var inputStream = SettingsMaintainer.class.getClassLoader().getResourceAsStream("TemplatesTable.txt")) {
            var reader = new BufferedReader(new InputStreamReader(inputStream));
            for(String url; (url = reader.readLine()) != null;) {
                templatesList.add(new Pair(url, SettingsMaintainer.class.getClassLoader().getResourceAsStream(url)));
            }
        } catch (IOException e) {
            gui.exceptionDialog("Nastala chyba při načítání obsahu z TemplatesTable.txt", e);
        }

        return templatesList;
    }


    /**
     * This method loads settings to ESRBase; It doesn't do any verification since the consistency has already been
     * thoroughly verified in verifySettings method
     */
    private static void loadLocalSettings(ESRBase base) {
        var gui = base.getGui();
        if(verifySettings(gui)) {
            try {
                var jaxbContext = JAXBContext.newInstance(Configuration.class);
                var unmarshaller = jaxbContext.createUnmarshaller();
                var conf = (Configuration) unmarshaller.unmarshal(configFilePath.toFile());

                base.setWorkingPath(conf.getWorkingPath());
                var links = conf.getLinksList();
                if(links.isEmpty()) {
                    gui.informDialog("Nebyly nalezeny žádné odkazy v konfiguračním souboru.");
                } else {
                    base.getWebLinks().addAll(links);
                }

                if(templatesFound) {
                    var templateFiles = Arrays.asList(new File(localSettingsPath + "\\Šablony\\").listFiles());
                    templateFiles.forEach(template -> base.getTemplates().add(template));
                }

                var holidays = conf.getHolidays();
                if(holidays.isEmpty()) {
                    gui.informDialog("V konfiguračním souboru nebyly nalezeny žádné státní svátky!");
                } else {
                    holidays.forEach(h -> DeadlineCalculator.addHolidayDate(h));
                }
                var sendersXmlFile = new File(localSettingsPath + "\\saved-senders.xml");
                if(sendersXmlFile.exists()) {
                    loadSavedSenders(sendersXmlFile, base);
                }
            } catch (JAXBException e) {
                gui.exceptionDialog("Došlo k chybě při parsování konfigurace pomocí JAXB", e);
            }

        } else {
            var rewrite = gui.yesOrNoDialog("Zapsání defaultní konfigurace",
                    "Nalezený konfigurační soubor není validní. " +
                            "Přejete si přepsat sočasnou konfiguraci defaultními soubory? Současná složka s konfigurací bude smazána a následně bude nakopírována " +
                            "defaultní konfigurace včetně šablon.", "Ano", "Ne");

            if(rewrite) {
                ESRTools.deleteDirectory(new File(String.valueOf(localSettingsPath)));
                setDefaultSettings(gui);
            } else {
                gui.errorDialog("Aplikace nemůže být spuštěna. Konfigurační soubor se můžete pokusit opravit ručně.");
                System.exit(0);
            }

        }

    }

    private static void loadSavedSenders(File sendersXmlFile, ESRBase base) {
        try (var sndrXsd = WorkingDataLoader.class.getClassLoader().getResourceAsStream("xsd-validators/default-senders.xsd")) {
            var jaxbContext = JAXBContext.newInstance(SendersWrapper.class);
            var unmarshaller = jaxbContext.createUnmarshaller();

            var tmpValidatorFile = new File(localSettingsPath + "\\tmp.xsd");
            Files.copy(sndrXsd, tmpValidatorFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

            var sendersSchema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                            .newSchema(tmpValidatorFile);
            unmarshaller.setSchema(sendersSchema);

            var sendersWrapper = (SendersWrapper) unmarshaller.unmarshal(sendersXmlFile);
            base.setSendersWrapper(sendersWrapper);
            Files.delete(tmpValidatorFile.toPath());
        } catch (JAXBException | SAXException e) {
            base.getGui().exceptionDialog("Nastala chyba při načítání uložených podatelů (senders) z XML", e);
        } catch (IOException e) {
            base.getGui().exceptionDialog("Nastala chyba při načítání kopírování/mazání validačního schématu pro default-senders.xml", e);
        }
    }

    public static void saveDefaultSenders(ESRBase base) {
        var gui = base.getGui();
        try {
            var jaxbContext = JAXBContext.newInstance(SendersWrapper.class);
            var marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            var sendersXmlFile = new File(localSettingsPath + "\\saved-senders.xml");
            marshaller.marshal(base.getSendersWrapper().get(), sendersXmlFile);
        } catch (JAXBException e) {
            gui.exceptionDialog("Došlo k chybě při ukládání informací o podatelích", e);
        }
    }

    /**
     * Goal of this method is to verify integrity of configuration files
     * returns false if config file is not valid
     * @return
     */
    private static boolean verifySettings(ESRApplication gui) {
        var templatesFolder = new File(localSettingsPath + "\\Šablony\\");
        if(templatesFolder.exists()) {
            if(templatesFolder.list().length == 0) {
                gui.informDialog("V '" + templatesFolder.getPath() + "' nebyly nalezeny žádné soubory šablony podání. Program bude i tak spuštěň.");
                templatesFound = false;
            } else templatesFound = true;

        } else {
            gui.errorDialog("Nebyla nalezena složka " + templatesFolder.getPath() +
                    "\nProgram bude spuštěn, ale nebudou načteny žádné šablony podání. Složku můžete vytvořit ručně.");
            templatesFound = false;
        }

        var configFile = configFilePath.toFile();

        if(configFile.exists()) {
            var docBuilderFactory = DocumentBuilderFactory.newInstance();

            try (var cfgValidatorStream = SettingsMaintainer.class.getClassLoader().getResourceAsStream("xsd-validators/config-validation.xsd")){
                var tmpValidator = Paths.get(templatesFolder.getPath() + "tmp.xsd");
                Files.copy(cfgValidatorStream, tmpValidator, StandardCopyOption.REPLACE_EXISTING);
                var docBuilder = docBuilderFactory.newDocumentBuilder();

                var configDocument = docBuilder.parse(configFile);
                var schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                var schema = schemaFactory.newSchema(new File(tmpValidator.toString()));
                Files.delete(tmpValidator);
                var validator = schema.newValidator();

                validator.validate(new DOMSource(configDocument));
            } catch (ParserConfigurationException | SAXException e) {
                gui.exceptionDialog("Nastala chyba XML parseru, pravděpodobně byla narušena integrita konfiguračních souborů.", e);
                return false;
            } catch (IOException e) {
                gui.exceptionDialog("Nastala IO výjimka", e);
                return false;
            }

        } else {
            gui.errorDialog("Nebyl nalezen konfigurační soubor " + configFile.getPath());
            return false;
        }

        return true;
    }

    public static void initLocalSettings(ESRBase base) {
        if(Files.exists(localSettingsPath)) {
            loadLocalSettings(base);
        } else {
            setDefaultSettings(base.getGui());

        }
    }

    /* This method is supposed to save last working path to XML. */
    public static void saveWorkingPath(Path workingPath, ESRApplication gui) {
        try (var inputStream = new FileInputStream(configFilePath.toString())){
            var documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            var config = documentBuilder.parse(inputStream);
            config.getElementsByTagName("workingpath").item(0).setTextContent(String.valueOf(workingPath));

            var transformer = TransformerFactory.newInstance().newTransformer();
            var domSrc = new DOMSource(config);
            transformer.transform(domSrc, new StreamResult(configFilePath.toFile()));

        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            gui.exceptionDialog("Chyba XML parseru při zápisu workingpath do config.xml", e);
        }

    }

    public static void saveLinks(ESRBase base) {
        var gui = base.getGui();
        var links = base.getWebLinks();
        try {
            var docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            var config = docBuilder.parse(configFilePath.toFile());
            config.normalize();

            var linksElement = (Element) config.getElementsByTagName("links").item(0);
            ESRTools.removeChildren(linksElement);

            links.forEach(lnk -> {
                var linkElement = config.createElement("link");
                var name = config.createAttribute("name");
                var url = config.createAttribute("url");
                name.setValue(lnk.getName());
                url.setValue(lnk.getUrl());
                linkElement.setAttributeNode(name);
                linkElement.setAttributeNode(url);

                linksElement.appendChild(linkElement);
            });

            var transformer = TransformerFactory.newDefaultInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            var domSrc = new DOMSource(config);
            var writer = new StringWriter();
            transformer.transform(domSrc, new StreamResult(writer));

            var outputXml = writer.toString().replaceAll("(?m)^[ \t]*\r?\n", "");

            Files.write(configFilePath, Collections.singleton(outputXml));

            gui.informDialog("Odkazy úspěšně aktualizovány!");
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            gui.exceptionDialog("Při ukládání změněných odkazů do konfiguračního souboru nastala chyba!", e);
        }
    }
}
