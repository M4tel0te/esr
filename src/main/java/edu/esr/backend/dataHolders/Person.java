package edu.esr.backend.dataHolders;

import edu.esr.backend.dataHolders.xmlBinding.OptionalStringAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Optional;

/**
 * @author Matyáš Vňuk
 * Class to hold data about person which migt be used in Case for holding data about its sender.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Person {
    @XmlElement
    private String name;
    @XmlElement
    private String streetAddress;
    @XmlElement
    private String cityAddress;
    @XmlElement
    private String birthDate;
    @XmlElement(name = "idDS", required = true)
    @XmlJavaTypeAdapter(OptionalStringAdapter.class)
    private Optional<String> dsID;

    public Person(String name, String streetAddress, String cityAddress, String birthDate, String dsID) {
        this.name = name;
        this.streetAddress = streetAddress;
        this.cityAddress = cityAddress;
        this.birthDate = birthDate;
        this.dsID = Optional.ofNullable(dsID);

    }

    public Person() {
        dsID = Optional.empty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public Optional<String> getDsID() {
        return dsID;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getCityAddress() {
        return cityAddress;
    }

    /**
     * This method is supposed to convert empty strings (which might be given from empty TextFields in GUI)
     * to nulls - this makes saving data into XML file easier. This method will be called only when Person class
     * is used to store Office data.
     */
    public void emptyToNull() {
        if(name != null && name.isEmpty()) this.name = null;
        if(streetAddress != null && streetAddress.isEmpty()) this.streetAddress = null;
        if(cityAddress != null && cityAddress.isEmpty()) this.cityAddress = null;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public void setCityAddress(String cityAddress) {
        this.cityAddress = cityAddress;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setDsID(String dsID) {
        this.dsID = Optional.ofNullable(dsID);
    }

    @Override
    public String toString() {
        return name + " - " + birthDate;
    }
}
