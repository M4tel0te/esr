package edu.esr.backend.dataHolders;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data storing immutable object
 */

@XmlRootElement(name = "link")
public class Link {
    @XmlAttribute(name = "name")
    private String name;
    @XmlAttribute(name = "url")
    private String url;

    public Link(String name, String url) {
        this.name = name;
        this.url = url;
    }
    /* Empty constructor is needed for JAXB */
    public Link() {

    }
    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return name + " [" + url + "]";
    }
}
