package edu.esr.backend.dataHolders;

import edu.esr.backend.ESRTools;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * @author Matyáš Vňuk
 */

public class Case {
    private Optional<Person> sender;
    private Person office;
    private LocalDate creationDate;
    private List<Act> actList;
    private Path directory;
    private String name;
    private CaseStatus status;
    private List<Deadline> deadlines;
    private Optional<String> fileNumber;

    public Case(String name, String strDirectory, String strStatus, String strCreationDate) {
        this.creationDate = LocalDate.parse(strCreationDate, DateTimeFormatter.ofPattern("d.M.y"));
        this.directory = Paths.get(strDirectory);
        this.name = name;
        this.status = parseStatus(strStatus);
        this.sender = Optional.empty();
        this.fileNumber = Optional.empty();;
    }

    /** Constructor for creation of a new case **/
    public Case(String name, String strDirectory, LocalDate creationDate) {
        this.creationDate = creationDate;
        this.directory = Paths.get(strDirectory);
        this.name = name;
        this.status = CaseStatus.IN_PROGRESS;
        this.sender = Optional.empty();
        this.fileNumber = Optional.empty();;
    }

    public static CaseStatus parseStatus(String strStatus) {
        if(strStatus.equals("W")) return CaseStatus.WIN;
        else if (strStatus.equals("L")) return CaseStatus.LOST;
        else if (strStatus.equals("P")) return CaseStatus.IN_PROGRESS;
        else return CaseStatus.OTHER;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Path getDirectory() {
        return directory;
    }

    public String getName() {
        return name;
    }

    public String getStatusFirstLetter() {
        if(this.status == CaseStatus.WIN) return "W";
        else if (this.status == CaseStatus.LOST) return "L";
        else if (this.status == CaseStatus.OTHER) return "O";
        else return "P";
    }

    public Optional<String> getFileNumber() {
        return fileNumber;
    }

    public int getStatusIndex() {
        switch (status) {
            case WIN:
                return 0;
            case LOST:
                return 1;
            case OTHER:
                return 2;
            default:
                return 3;
        }
    }

    public void setStatusByIndex(int index) {
        switch (index) {
            case 0:
                this.status = CaseStatus.WIN;
                break;
            case 1:
                this.status = CaseStatus.LOST;
                break;
            case 2:
                this.status = CaseStatus.OTHER;
                break;
            default:
                this.status = CaseStatus.IN_PROGRESS;
                break;
        }
    }

    public boolean deleteAct(Act toBeDeleted) {
        if(actList.contains(toBeDeleted)) {
            if(toBeDeleted.getActPath().toFile().exists()) {
                toBeDeleted.deleteAllActFiles();
            }
            actList.remove(toBeDeleted);
            return true;

        }
        return false;
    }

    public boolean wipeFiles() {
        return ESRTools.deleteDirectory(directory.toFile());
    }

    public void setSender(Person s) {
        this.sender = Optional.ofNullable(s);
    }

    public void setOffice(Person o) {
        this.office = o;
    }

    public void setFileNumber(String number) {
        fileNumber = Optional.ofNullable(number);
    }

    public Person getOffice() {
        return office;
    }

    public void setActList(List<Act> actList) {
        this.actList = actList;
    }

    public List<Act> getActList() {
        return actList;
    }

    public Optional<Person> getSender() {
        return sender;
    }

    public List<Deadline> getDeadlines() {
        return deadlines;
    }

    public void setDeadlines(List<Deadline> deadlines) {
        this.deadlines = deadlines;
    }

    @Override
    public String toString() {
        return "[" + getStatusFirstLetter() + "] " + name;
    }
}
