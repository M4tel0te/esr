package edu.esr.backend.dataHolders;

public enum ActType {
    RECEIVE,
    SEND,
    OTHER
}
