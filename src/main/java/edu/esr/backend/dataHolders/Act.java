package edu.esr.backend.dataHolders;

import edu.esr.backend.ESRTools;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Matyáš Vňuk
 */

public class Act {
    private ActType actType;
    private List<ActFile> fileList;
    private Path actPath;
    private String name;
    private LocalDate actDate;
    private String description;

    public Act(String name, String description, String actType, Path actPath, String strActDate) {
        this.actType = parseType(actType);
        this.actPath = actPath;
        this.name = name;
        this.actDate = LocalDate.parse(strActDate, DateTimeFormatter.ofPattern("d.M.y"));
        this.description = description;
        this.fileList = null;
    }
    public Act(String name, String description, ActType actType, Path actPath, LocalDate actDate) {
        this.actType = actType;
        this.actPath = actPath;
        this.name = name;
        this.actDate = actDate;
        this.description = description;
        this.fileList = null;
    }

    private ActType parseType(String s) {
        if(s.equals("S")) return ActType.SEND;
        else if(s.equals("R")) return ActType.RECEIVE;
        else return ActType.OTHER;
    }

    public ActType getActType() {
        return actType;
    }

    public List<ActFile> getFileList() {
        return fileList;
    }

    public Path getActPath() {
        return actPath;
    }

    public String getName() {
        return name;
    }

    public LocalDate getActDate() {
        return actDate;
    }

    public String getDescription() {
        return description;
    }

    public void setActPath(Path actPath) {
        this.actPath = actPath;
    }

    /** Goal of this method is to load up info about act's files when the object is initiated.
     * It is already ensured that the folder exists.
     */
    private List<ActFile> initFiles(Path actPath) {
        var filesArray = new File(String.valueOf(actPath)).listFiles();
        if(filesArray != null) {
            var actFiles = new ArrayList<ActFile>();
            Arrays.stream(filesArray).forEach(f -> actFiles.add(new ActFile(f)));
            return actFiles;
        } else return null;
    }

    public boolean deleteAllActFiles() {
        return ESRTools.deleteDirectory(actPath.toFile());
    }

    public void reloadFiles() {
        this.fileList = initFiles(actPath);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setActType(ActType actType) {
        this.actType = actType;
    }

    public void setActDate(LocalDate actDate) {
        this.actDate = actDate;
    }

    public static ActType actTypeByIndex(int index) {
        switch (index) {
            case 0:
                return ActType.SEND;
            case 1:
                return ActType.RECEIVE;
            default:
                return ActType.OTHER;
        }
    }

    public static int actIndexByType(ActType type) {
        switch (type) {
            case SEND:
                return 0;
            case RECEIVE:
                return 1;
            default:
                return 2;
        }
    }

    public int getTypeIndex() {
        return actIndexByType(actType);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypesFirstLetter() {
        switch (actType) {
            case RECEIVE:
                return "R";
            case SEND:
                return "S";
            default:
                return "O";
        }
    }

    @Override
    public String toString() {
        var temp = new String();
        switch (actType) {
            case RECEIVE:
                temp = "[D] ";
                break;
            case SEND:
                temp = "[P] ";
                break;
            case OTHER:
                temp = "[J] ";
                break;
        }
        temp = temp + this.name;
        return temp;
    }
}
