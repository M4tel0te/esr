package edu.esr.backend.dataHolders;

import java.io.File;

/**
 * This class is simply a File class with its own overriden toString method so it can be easily added to
 * ListView in GUI
 */
public class ActFile extends File {
    public ActFile(File file) {
        super(file.getPath());
    }

    @Override
    public String toString() {
        var displayedName = new String();
        var lastIndex = this.getName().lastIndexOf(".");
        if(lastIndex == -1) {
            displayedName = "[-] " + this.getName();
        } else displayedName = "[" + this.getName().substring(lastIndex+1).toUpperCase() + "] " +
                this.getName().substring(0, this.getName().lastIndexOf('.'));
        return displayedName;
    }
}
