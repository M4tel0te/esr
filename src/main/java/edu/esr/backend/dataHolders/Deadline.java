package edu.esr.backend.dataHolders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


public class Deadline {
    private LocalDate date;
    private String name;
    private DeadlineType type;
    private int daysRemaining;

    /** Constructor for loading a deadline from text (XML) **/
    public Deadline(String dateString, String name, String type) {
        this.date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("d.M.y"));;
        this.name = name;
        this.type = parseType(type);
        this.daysRemaining = (int) ChronoUnit.DAYS.between(LocalDate.now(), date);
    }

    /** Constructor for adding a new deadline via GUI **/
    public Deadline(LocalDate date, String name, DeadlineType type) {
        this.date = date;
        this.name = name;
        this.type = type;
        this.daysRemaining = (int) ChronoUnit.DAYS.between(LocalDate.now(), date);
    }

    private DeadlineType parseType(String str) {
        switch(str) {
            case "O":
                return DeadlineType.OFFICE;
            default:
                return DeadlineType.USER;
        }
    }

    @Override
    public String toString() {
        var builder = new StringBuilder("[");
        if(this.type == DeadlineType.OFFICE) builder.append("Ú] ");
        else builder.append("J] ");
        builder.append(date.format(DateTimeFormatter.ofPattern("dd.MM.y")));
        builder.append(" - ");
        builder.append(name);
        if(daysRemaining > -1) builder.append(" (" + daysRemaining + " dnů zbývá)");
        return builder.toString();
    }

    public String getTypesFirstLetter() {
        if(this.type == DeadlineType.OFFICE) return "O";
        else return "U";
    }
    public int getDaysRemaining() {
        return daysRemaining;
    }

    public DeadlineType getType() {
        return type;
    }

    public LocalDate getDate() {
        return date;
    }

    /**
     * This method is supposed for editing values of already existing deadline.
     */
    public void resetValues(LocalDate date, String name, DeadlineType type) {
        this.date = date;
        this.name = name;
        this.type = type;
        this.daysRemaining = (int) ChronoUnit.DAYS.between(LocalDate.now(), date);
    }

    public String getName() {
        return name;
    }
}
