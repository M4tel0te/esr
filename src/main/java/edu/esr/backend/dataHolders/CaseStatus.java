package edu.esr.backend.dataHolders;

public enum CaseStatus {
    WIN,
    LOST,
    IN_PROGRESS,
    OTHER;
}
