package edu.esr.backend.dataHolders.xmlBinding;

import edu.esr.backend.dataHolders.Person;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "senders")
public class SendersWrapper {
    @XmlElement(name = "sender")
    private List<Person> senders;

    public List<Person> getSenders() {
        return senders;
    }

    public SendersWrapper() {
        senders = new ArrayList<>();
    }

    public SendersWrapper(List<Person> list) {
        this.senders = list;
    }
}
