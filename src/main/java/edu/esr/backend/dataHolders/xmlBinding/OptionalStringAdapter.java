package edu.esr.backend.dataHolders.xmlBinding;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Optional;

/**
 * This class is an Adapter for marshalling optional Strings since JAXB does not support Optionals
 */

public class OptionalStringAdapter extends XmlAdapter<String, Optional<String>> {
    @Override
    public Optional<String> unmarshal(String s) throws Exception {
        if(s.equals("")) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(s);
        }
    }

    @Override
    public String marshal(Optional<String> s) throws Exception {
        if(s.isPresent()) {
            return s.get();
        } else {
            return null;
        }
    }
}
