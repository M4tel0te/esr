package edu.esr.backend.dataHolders.xmlBinding;

import edu.esr.backend.dataHolders.Case;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for JAXB Marshalling of the basic information about cases in .esr
 */

@XmlRootElement(name = "cases")
public class CasesWrapper {
    @XmlElement(name = "case")
    private List<CaseSimplified> caseDTOList;

    public CasesWrapper() {
        caseDTOList = new ArrayList<>();
    }

    public List<CaseSimplified> getCaseDTOList() {
        return caseDTOList;
    }

    public void addNewCase(Case cs) {
        this.caseDTOList.add(new CaseSimplified(cs.getName(), cs.getDirectory().toFile().getName(),
                cs.getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyy"))));
    }
    public void removeCase(Case cs) {
        caseDTOList.removeIf(cDO -> cDO.getCaseDirectory().equals(cs.getDirectory().toFile().getName()));
    }
}

