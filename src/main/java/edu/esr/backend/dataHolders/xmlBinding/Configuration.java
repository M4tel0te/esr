package edu.esr.backend.dataHolders.xmlBinding;

import edu.esr.backend.dataHolders.Link;

import javax.xml.bind.annotation.*;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;

/**
 * DTO object for loading & saving of config by JAXB
 */

@XmlRootElement(name = "config")
public class Configuration {
    @XmlElement(name = "workingpath")
    private String workingPath;
    @XmlElementWrapper(name = "links")
    @XmlElement(name = "link")
    private List<Link> linksList;
    @XmlElementWrapper(name = "holidays")
    @XmlElement(name = "date")
    private List<String> holidays;

    public Configuration() {
    }



    public Path getWorkingPath() {
        if(workingPath.length() == 0) {
            return null;
        } else {
            return Paths.get(workingPath);
        }
    }

    public List<Link> getLinksList() {
        return linksList;
    }

    public List<String> getHolidays() {
        return holidays;
    }
}
