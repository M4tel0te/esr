package edu.esr.backend.dataHolders.xmlBinding;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is just a data object for holding just basic information about unloaded cases.
 */

@XmlRootElement(name = "case")
public class CaseSimplified {
    @XmlElement
    private String name;
    @XmlElement
    private String caseDirectory;
    @XmlElement
    private String creationDate;
    @XmlAttribute
    private String status;

    public CaseSimplified() {

    }

    /**
     * Constructor used when adding a new case
     */
    public CaseSimplified(String name, String caseDirectory, String creationDate) {
        this.name = name;
        this.caseDirectory = caseDirectory;
        this.creationDate = creationDate;
        this.status = "P";
    }

    public String getName() {
        return name;
    }

    public String getCaseDirectory() {
        return caseDirectory;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getStatus() {
        return status;
    }
}