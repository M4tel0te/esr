package edu.esr.backend;

import edu.esr.core.ESRApplication;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Matyáš Vňuk
 * This class should include methods which are gonna either create or verify the project (working) folder
 * The loading procedure is left to WorkingDataLoader.class
 */

public class WorkingDirectoryMaintainer {

    public static void loadWorkingDirectory(Path workingPath, ESRApplication gui) {
        if(workingPath != null) {
            var workingDirectory = new File(String.valueOf(workingPath));
            if(workingDirectory.exists() && workingDirectory.isDirectory()) {
                WorkingDataLoader.loadDataFromWorkingDirectory(workingPath, gui);
            }
            else {
                var createNew = gui.yesOrNoDialog("Chyba při načítání pracovní složky", "Poslední načtená pracovní složka (" + workingPath + ") nebyla nalezena. " +
                        "Přejete si vytvořit novou (nebo načíst jinou stávající) pracovní složku?", "Ano", "Ne");
                if(createNew) {
                    loadWorkingDirectory(null, gui);
                } else {
                    gui.errorDialog("Aplikace nemůže být spuštěna. Můžete se pokusit složku opravit ručně.");
                    System.exit(0);
                }
            }


        } else {
            var createNew = gui.yesOrNoDialog("Cesta k pracovní složce nenalezena",
                    "V místní konfiguraci nebyla nalezena cesta k pracovní složce. Přejete si načíst existující složku, nebo vytvořit novou?",
                    "Vytvořit novou", "Načíst stávající");
            if(createNew) {
                if(createNewWorkingDirectory(gui));
                else {
                    gui.errorDialog("Aplikace bude ukončena.");
                    System.exit(0);
                }
            }
            else choseExistingWorkingDirectory(gui);
        }
    }

    public static boolean createNewWorkingDirectory(ESRApplication gui) {
        gui.informDialog("Vytvořte ve vašem počítači prázdnou složku, ve které budou uložena veškerá data všech případů. Tuto složku následně zvolte.");
        var dirChoser = new DirectoryChooser();
        dirChoser.setTitle("Zvolte existující složku pro vytvoření projektu");

        var selectedFolder = dirChoser.showDialog(gui.getStage());
        if(selectedFolder != null) {
            var workingPath = Paths.get(selectedFolder.getPath());
            try {
                Files.copy(WorkingDirectoryMaintainer.class.getClassLoader().getResourceAsStream("project.esr"),
                        Paths.get(workingPath + "\\project.esr"));
                gui.informDialog("Složka " + workingPath + " inicializována jako pracovní. Složku můžete přenášet, zálohovat, šifrovat, ale ručním zásahům se " +
                        "raději vyvarujte. V případě porušení integrity sice o data nepřijdete, aplikace by však mohla mít potíže s jejich načtením.");
                SettingsMaintainer.saveWorkingPath(workingPath, gui);
                gui.getBase().setWorkingPath(workingPath);
                gui.getBase().getCases().clear();

            } catch (IOException e) {
                gui.exceptionDialog("Neyblo možné vytvořit projektový soubor ve vámi zvolené složce. Máte dostatečná oprávnění?", e);
                loadWorkingDirectory(workingPath, gui);
            }
        } else {
            if (gui.yesOrNoDialog("Chyba při výběru složky", "Nebyla zvolena žádná složka. Přejete si výběr opakovat?",
                    "Ano", "Ne")) createNewWorkingDirectory(gui);
            else {
                return false;
            }
        }
        return true;
    }

    public static void choseExistingWorkingDirectory(ESRApplication gui) {
        gui.informDialog("Najděte složku s projektovým souborem, která obsahuje soubor project.esr a zvolte ho!");
        var fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("ESŘ projekt (project.esr)", "project.esr"));
        fileChooser.setTitle("Zvolte existující projektovou složku");
        /** There is no option that user would choose non-existing folder. **/
        var folder = fileChooser.showOpenDialog(gui.getStage());
        if(folder != null) {
            WorkingDataLoader.loadDataFromWorkingDirectory(folder.toPath().getParent(), gui);
            SettingsMaintainer.saveWorkingPath(folder.toPath().getParent(), gui);
        } else {
            if(gui.yesOrNoDialog("Chyba při výběru souboru", "Neybl zvolen žádný soubor. Přejete si výběr opakovat?",
                    "Ano", "Ne")) choseExistingWorkingDirectory(gui);
            else {
                if(!gui.getBase().getCases().isEmpty()) {
                    gui.informDialog("Jiná složka nebude načtena.");
                } else {
                    gui.errorDialog("Aplikace bude ukončena.");
                    System.exit(0);
                }

            }

        }

    }


}
